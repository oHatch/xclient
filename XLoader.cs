using System;
using VRC;
using X.Stuff;
using X.Stuff.X;
using X.Stuff.XESP;
using X.Stuff.Avatars;
using UnityEngine;
using System.Collections;

namespace X
{
    public class XLoader : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(this);
            XUI.InitializeWindow();
            XUI.InitializeBGs();

            Variables.oldMainWindowRectHeight = Variables.MainWindowRect.height;
        }

        private void Start()
        {
            StartCoroutine(X.Stuff.Caching.ScanAll());
            StartCoroutine(X.Stuff.Caching.CacheAvatarIDs());
            StartCoroutine(X.Stuff.Caching.CacheAvatarIDsV2());
            StartCoroutine(X.Stuff.Avatars.AvatarsCache.RunChecks());
            //StartCoroutine(X.Stuff.Caching.CacheWorldIDs());
            //Needs fixing, when loading into room -> gamecrash

            GameObject go = new GameObject();
            Variables.thisGO = go;
            go.AddComponent<Flying>();
            go.AddComponent<BoneESP>();
            go.AddComponent<ESPType>();
            go.AddComponent<PortalMagic>();
            //go.AddComponent<AvatarsCache>();

            Application.targetFrameRate = 200;
        }

        private void Update()
        {
            if (Event.current.shift && Input.GetKeyDown(KeyCode.Alpha5))
            {
                Variables.ShowMenu = !Variables.ShowMenu;
                if (Variables.ShowMenu)
                    GUI.FocusWindow(0);
            }

            if (Event.current.shift && Input.GetKeyDown(KeyCode.Alpha1))
                Log.Avatar();

            if (Event.current.shift && Input.GetKeyDown(KeyCode.Alpha2))
                Log.Player();

            if (Event.current.shift && Input.GetKeyDown(KeyCode.Alpha3))
                Log.World();

            if (Event.current.shift && Input.GetKeyDown(KeyCode.Alpha4))
                Functions.TeleportPug();

            if (Variables.playDespacito)
            {
                Variables.playDespacito = !Variables.playDespacito;
                StartCoroutine(Functions.AlexaLoadDespacito(Constants.despacitoClipHelperPath));
            }

            if (!Variables.ShowMenu && Variables.ShowPlayers)
                Variables.ShowPlayers = !Variables.ShowPlayers;

            if (Variables.currentInputController == null)
            {
                if (APMDPMJMOCD.IPPOKDOLGFC)
                {
                    VRCPlayer vrcPlayer = PlayerManager.GetCurrentPlayer()?.vrcPlayer;
                    Variables.currentInputController = vrcPlayer.GetComponent<LocomotionInputController>();
                }
            }
            if (Event.current.shift && Input.GetKeyDown(KeyCode.Alpha7))
                Debug.Log("Path: " + Constants.appPath);

            if (Variables.simpleAimBot)
                Functions.executeAimBot(Variables.playerAimbot);

            if (Event.current.shift && Input.GetKeyDown(KeyCode.Alpha6))
                Functions.EnableAdminAccess();

            if (Event.current.control && Input.GetKeyDown(KeyCode.O))
                Functions.GoHome();

            if (Event.current.control && Input.GetKeyDown(KeyCode.K))
                Functions.JoinWorldInstance("wrld_496b11e8-25a0-4f35-976d-faae5e00d60e:91367~hidden(usr_5d0b3397-3d3c-4bdc-b6d8-07f434c5f8d0)~nonce(4E62185B422748CD804234B70D4148A3FCEE8B941E3EAD620B2623219FC5CE5C)");
        }

        private void OnGUI()
        {
            if (Variables.Button == null && Variables.WindowT != null)
            { 
                XUI.InitializeUI();
                XUI.ApplySkins();
            }

            if (Variables.MainWindowRect != null && Variables.ShowMenu && Variables.WindowBG != null)
                Variables.MainWindowRect = GUI.Window(0, Variables.MainWindowRect, Functions.OnMainWindowOpen, string.Empty, Variables.WindowBG);
        }

        private void OnDestroy()
        {
            Functions.WriteLogs();
        }
    }
}