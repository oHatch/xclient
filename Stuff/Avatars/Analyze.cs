﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
using VRC;
using X.Stuff.X;

namespace X.Stuff.Avatars
{
    public class Analyze
    {
        public static void Components(Component component, StreamWriter writer, string indent)
        {
            writer.WriteLine("{0}{1}", indent, (component == null) ? "(null)" : component.GetType().Name);
        }

        public static void GameObject(GameObject gameObject, StreamWriter writer, string indent)
        {
            writer.WriteLine("{0}+{1}", indent, gameObject.name);
            Component[] components = gameObject.GetComponents<Component>();
            for (int i = 0; i < components.Length; i++)
            {
                Analyze.Components(components[i], writer, indent + "\t");
            }
            foreach (object obj in gameObject.transform)
            {
                Analyze.GameObject(((Transform)obj).gameObject, writer, indent + "\t");
            }
        }

        public static void Shaders(GameObject gameObject, Player player)
        {
            string path = Constants.analyzedPlayerPath + player.DPHNAOBNDJD?.displayName + ".txt";
            File.AppendAllText(path, Environment.NewLine + "Shaders:" + Environment.NewLine);
            Renderer[] rendererArray = gameObject.GetComponentsInChildren<Renderer>(true);
            foreach (Renderer renderer in rendererArray)
            {
                Material[] materials = renderer.materials;
                foreach (Material material in materials)
                {
                    string shadername = material.shader.name;
                    File.AppendAllText(path, shadername + Environment.NewLine);
                }
            }
        }

        public static void ThisPlayer(Player player)
        {
            string savedFile = Constants.analyzedPlayerPath + player.DPHNAOBNDJD?.displayName + ".txt";
            using (StreamWriter streamWriter = new StreamWriter(savedFile, false))
            {
                Analyze.GameObject(player.gameObject, streamWriter, string.Empty);
            }
            Analyze.Shaders(player.gameObject, player);
        }
    }
}