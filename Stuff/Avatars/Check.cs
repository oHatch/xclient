﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRC;
using VRC.Core;
using X.Stuff.X;

namespace X.Stuff.Avatars
{
    public class Check
    {
        /*public static IEnumerator IfStolen()
        {
            while (true)
            {
                if (APMDPMJMOCD.IPPOKDOLGFC)
                {
                    var players = PlayerManager.GetAllPlayers();
                    if (players.Length > 0)
                        foreach (Player player in players)
                        {
                            if (player == null)
                                continue;
                            if (player.LBHEAAOFFOA) //if player is Loaded (necessary for PipelineManager)
                            {
                                if (player.GetComponentInChildren<PipelineManager>() && player.DPHNAOBNDJD != null && player.vrcPlayer != null)
                                {
                                    try
                                    {
                                        ApiAvatar currentUserAA = player.vrcPlayer.IIAMBAECJEM?.IOMMPLNNLDO;
                                        ApiAvatar vrcaAvatar;
                                        string displayName = player.DPHNAOBNDJD.displayName;

                                        PipelineManager pM = player.GetComponentInChildren<PipelineManager>();
                                        if (string.IsNullOrEmpty(pM.blueprintId))
                                        {
                                            Debug.Log("Could not run check on Player " + displayName + ", there was no BlueprintID in the PipelineManager.");
                                            continue;
                                        }
                                        else
                                        {
                                            API.Fetch<ApiAvatar>(pM.blueprintId,
                                                delegate (ApiContainer c)
                                                    {
                                                        vrcaAvatar = (ApiAvatar)c.Model;

                                                        if (currentUserAA.id != pM.blueprintId && vrcaAvatar.releaseStatus == "private")
                                                        {
                                                            player.vrcPlayer.SetNamePlateColor(Color.cyan);
                                                            Debug.Log("Mismatch ( currentID: " + currentUserAA.id + " | vrcaAvatarID: " + pM.blueprintId + " )!");
                                                            Debug.Log("<color=red> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- stole Avatar: " + pM.blueprintId + " </color>");
                                                            Debug.Log("Player " + displayName + " might be a stealer! Logged to DB.");
                                                        }
                                                        else if (player.DPHNAOBNDJD.id != vrcaAvatar.authorId && vrcaAvatar.releaseStatus == "private")
                                                        {
                                                            player.vrcPlayer.SetNamePlateColor(Color.cyan);
                                                            Debug.Log("Mismatch ( currentUID: " + player.DPHNAOBNDJD.id + " | vrcaAvatarUID: " + vrcaAvatar.authorId + " )!");
                                                            Debug.Log("<color=red> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- stole Avatar: " + pM.blueprintId + " </color>");
                                                            Debug.Log("Player " + displayName + " might be a stealer! Logged to DB.");
                                                        }
                                                        else
                                                        {
                                                            Debug.Log("Player " + displayName + " is not stealing!");
                                                            if (pM.blueprintId == currentUserAA.id)
                                                            {
                                                                Debug.Log("<color=green> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- VRCAAvatar: " + pM.blueprintId + " -+- equals current Avatar: " + currentUserAA.id + " </color>");
                                                                Debug.Log("Shared AssetURL: " + vrcaAvatar.assetUrl);
                                                            }
                                                            else
                                                            {
                                                                Debug.Log("<color=green> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- VRCAAvatar: " + pM.blueprintId + " -+- is different from current Avatar: " + currentUserAA.id + " </color>");
                                                                Debug.Log("VRCAAvatar is public and was reuploaded as " + currentUserAA.releaseStatus);
                                                            }
                                                        }
                                                    },
                                                delegate (ApiContainer b)
                                                    {
                                                        Debug.Log("Something failed!");
                                                        Debug.Log("1. ApiFields couldn't be decoded \n 2. Avatar couldn't be downloaded \n  3. Avatar does not exist on the Avatar-API anymore");
                                                    }, false);
                                        }
                                    }
                                    catch (Exception xX)
                                    {
                                        Debug.Log(xX.Message);
                                        Debug.Log(xX);
                                    }

                                    yield return new WaitForSeconds(0f);
                                }
                            }
                        }
                }
                yield return new WaitForEndOfFrame();
            }
        }*/

        public static void IfStolenSingle(Player player)
        {
            if (player.LBHEAAOFFOA) //if player is Loaded (necessary for PipelineManager)
            {
                if (player.GetComponentInChildren<PipelineManager>() && player.DPHNAOBNDJD != null && player.vrcPlayer != null)
                {
                    try
                    {
                        ApiAvatar currentUserAA = player.vrcPlayer.IIAMBAECJEM?.IOMMPLNNLDO;
                        ApiAvatar vrcaAvatar;
                        string displayName = player.DPHNAOBNDJD.displayName;

                        PipelineManager pM = player.GetComponentInChildren<PipelineManager>();
                        if (string.IsNullOrEmpty(pM.blueprintId))
                        {
                            player.vrcPlayer.SetNamePlateColor(Color.red);
                            Debug.Log("Could not run check on Player " + displayName + ", there was no BlueprintID in the PipelineManager.");
                            Variables.InvalidAvatars.Add(currentUserAA.id, player.DPHNAOBNDJD.id);
                            Debug.Log("Player " + displayName + " was sent to TryAgain()!");
                            Check.TryAgain(player);
                            return;
                        }
                        else
                        {
                            API.Fetch<ApiAvatar>(pM.blueprintId,
                                delegate (ApiContainer c)
                                {
                                    vrcaAvatar = (ApiAvatar)c.Model;

                                    if (currentUserAA.id != pM.blueprintId && vrcaAvatar.releaseStatus == "private")
                                    {
                                        player.vrcPlayer.SetNamePlateColor(Color.cyan);
                                        Debug.Log("Mismatch ( currentID: " + currentUserAA.id + " | vrcaAvatarID: " + pM.blueprintId + " | VRCAAvatar ReleaseStatus: " + vrcaAvatar.releaseStatus + " )! Detection Method(1)");
                                        Debug.Log("<color=red> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- stole Avatar: " + pM.blueprintId + " </color>");
                                        Debug.Log("Player " + displayName + " might be a stealer! Logged to DB. db today xd");
                                    }
                                    else if (currentUserAA.id != pM.blueprintId && vrcaAvatar.releaseStatus == "private" && player.DPHNAOBNDJD.id != vrcaAvatar.authorId)
                                    {
                                        player.vrcPlayer.SetNamePlateColor(Color.cyan);
                                        Debug.Log("Mismatch ( currentID: " + currentUserAA.id + " | vrcaAvatarID: " + pM.blueprintId + " | VRCAAvatar ReleaseStatus: " + vrcaAvatar.releaseStatus + " )! Detection Method(2)");
                                        Debug.Log("<color=red> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- stole Avatar: " + pM.blueprintId + " </color>");
                                        Debug.Log("Player " + displayName + " is a stealer! Logged to DB. db today xd");
                                    }
                                    else if (player.DPHNAOBNDJD.id != vrcaAvatar.authorId && vrcaAvatar.releaseStatus == "private")
                                    {
                                        player.vrcPlayer.SetNamePlateColor(Color.cyan);
                                        Debug.Log("Mismatch ( currentUID: " + player.DPHNAOBNDJD.id + " | vrcaAvatarUID: " + vrcaAvatar.authorId + " | VRCAAvatar ReleaseStatus: " + vrcaAvatar.releaseStatus + " )! Detection Method(3)");
                                        Debug.Log("<color=red> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- stole Avatar: " + pM.blueprintId + " </color>");
                                        Debug.Log("Player " + displayName + " is a stealer! Logged to DB. db today xd");
                                    }
                                    else
                                    {
                                        player.vrcPlayer.RestoreNamePlateColor();
                                        Debug.Log("Player " + displayName + " is not stealing!");
                                        if (pM.blueprintId == currentUserAA.id)
                                        {
                                            Debug.Log("<color=green> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- VRCAAvatar: " + pM.blueprintId + " -+- equals current Avatar: " + currentUserAA.id + " </color>");
                                            Debug.Log("Shared AssetURL: " + vrcaAvatar.assetUrl);
                                        }
                                        else
                                        {
                                            Debug.Log("<color=green> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- VRCAAvatar: " + pM.blueprintId + " -+- is different from current Avatar: " + currentUserAA.id + " </color>");
                                            Debug.Log("VRCAAvatar is public and was reuploaded as " + currentUserAA.releaseStatus);
                                        }
                                    }
                                },
                                delegate (ApiContainer b)
                                {
                                    player.vrcPlayer.SetNamePlateColor(new Color(1f, 0.65f, 0f, 0f));
                                    Debug.Log("Something failed!");
                                    Debug.Log("1. ApiFields couldn't be decoded \n 2. Avatar couldn't be downloaded \n  3. Avatar does not exist on the Avatar-API anymore" + Environment.NewLine);
                                    Debug.Log(b.Text + Environment.NewLine);
                                    Debug.Log(b.Error + Environment.NewLine);
                                }, false);
                        }
                    }
                    catch (Exception xX)
                    {
                        Debug.Log(xX.Message);
                        Debug.Log(xX);
                    }
                }
            }
        }

        public static void TryAgain(Player player)
        {
            try
            {
                ApiAvatar currentUserAA = player.vrcPlayer.IIAMBAECJEM?.IOMMPLNNLDO;
                ApiAvatar vrcaAvatar;
                string displayName = player.DPHNAOBNDJD.displayName;

                PipelineManager pM = player.GetComponentInChildren<PipelineManager>();
                if (string.IsNullOrEmpty(pM.blueprintId))
                {
                    player.vrcPlayer.SetNamePlateColor(new Color(0.75f, 0.1f, 0.1f, 1f));
                    Debug.Log("Tried to run check on Player " + displayName + " again, there was no BlueprintID in the PipelineManager.");
                    return;
                }
                else
                {
                    API.Fetch<ApiAvatar>(pM.blueprintId,
                        delegate (ApiContainer c)
                        {
                            vrcaAvatar = (ApiAvatar)c.Model;

                            if (currentUserAA.id != pM.blueprintId && vrcaAvatar.releaseStatus == "private")
                            {
                                player.vrcPlayer.SetNamePlateColor(Color.cyan);
                                Debug.Log("Mismatch ( currentID: " + currentUserAA.id + " | vrcaAvatarID: " + pM.blueprintId + " | VRCAAvatar ReleaseStatus: " + vrcaAvatar.releaseStatus + " )! Detection Method(1)T");
                                Debug.Log("<color=red> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- stole Avatar: " + pM.blueprintId + " </color>");
                                Debug.Log("Player " + displayName + " might be a stealer! Logged to DB. db today xd");
                            }
                            else if (currentUserAA.id != pM.blueprintId && vrcaAvatar.releaseStatus == "private" && player.DPHNAOBNDJD.id != vrcaAvatar.authorId)
                            {
                                player.vrcPlayer.SetNamePlateColor(Color.cyan);
                                Debug.Log("Mismatch ( currentID: " + currentUserAA.id + " | vrcaAvatarID: " + pM.blueprintId + " | VRCAAvatar ReleaseStatus: " + vrcaAvatar.releaseStatus + " )! Detection Method(2)T");
                                Debug.Log("<color=red> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- stole Avatar: " + pM.blueprintId + " </color>");
                                Debug.Log("Player " + displayName + " is a stealer! Logged to DB. db today xd");
                            }
                            else if (player.DPHNAOBNDJD.id != vrcaAvatar.authorId && vrcaAvatar.releaseStatus == "private")
                            {
                                player.vrcPlayer.SetNamePlateColor(Color.cyan);
                                Debug.Log("Mismatch ( currentUID: " + player.DPHNAOBNDJD.id + " | vrcaAvatarUID: " + vrcaAvatar.authorId + " | VRCAAvatar ReleaseStatus: " + vrcaAvatar.releaseStatus + " )! Detection Method(3)T");
                                Debug.Log("<color=red> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- stole Avatar: " + pM.blueprintId + " </color>");
                                Debug.Log("Player " + displayName + " is a stealer! Logged to DB. db today xd");
                            }
                            else
                            {
                                player.vrcPlayer.RestoreNamePlateColor();
                                Debug.Log("Player " + displayName + " is not stealing!");
                                if (pM.blueprintId == currentUserAA.id)
                                {
                                    Debug.Log("<color=green> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- VRCAAvatar: " + pM.blueprintId + " -+- equals current Avatar: " + currentUserAA.id + " </color>");
                                    Debug.Log("Shared AssetURL: " + vrcaAvatar.assetUrl);
                                }
                                else
                                {
                                    Debug.Log("<color=green> Data: " + displayName + " -+- " + player.DPHNAOBNDJD.id + " -+- steamUserID " + player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString() + " -+- VRCAAvatar: " + pM.blueprintId + " -+- is different from current Avatar: " + currentUserAA.id + " </color>");
                                    Debug.Log("VRCAAvatar is public and was reuploaded as " + currentUserAA.releaseStatus);
                                }
                            }
                        },
                        delegate (ApiContainer b)
                        {
                            player.vrcPlayer.SetNamePlateColor(new Color(1f, 0.65f, 0f, 0f));
                            Debug.Log("Something failed!");
                            Debug.Log("1. ApiFields couldn't be decoded \n 2. Avatar couldn't be downloaded \n  3. Avatar does not exist on the Avatar-API anymore" + Environment.NewLine);
                            Debug.Log(b.Text + Environment.NewLine);
                            Debug.Log(b.Error + Environment.NewLine);
                        }, false);
                }
            }
            catch (Exception ttt)
            {
                Debug.Log(ttt.Message);
                Debug.Log(ttt);
            }
        }
    }
}