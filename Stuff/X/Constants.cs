﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace X.Stuff.X
{
    public class Constants
    {
        public const string avatarLogPath = @"X\Avatars\Avatars.txt";
        public const string avatarLogV2Path = @"X\Avatars\AvatarsV2.txt";
        public const string VRCAPath = @"X\Avatars\Files\";
        public const string worldLogPath = @"X\Worlds\Worlds.txt";
        public const string shaderListPath = @"X\Lists\Shaders.txt";
        public const string audioClipListPath = @"X\Lists\AudioClips.txt";
        public const string gameObjectListPath = @"X\Lists\GameObjects.txt";
        public const string invalidAvatarsLogPath = @"X\Avatars\InvalidAvatars.txt";
        public const string despacitoClipPath = "F:/SteamLibrary/steamapps/common/VRChat/X/Despacito.wav";
        public const string despacitoClipHelperPath = "file:///" + Constants.despacitoClipPath;
        public const string assetBundlePath = "https://api.vrchat.cloud/api/1/file/file_c6c38dc7-2e5f-4770-9e7c-7f61f8d3985b/1/file";
        public const string assetBundlePathHelper = "file:///" + Constants.assetBundlePath;
        public const string avatarUrl = @"https://cloud.naatiivee-works.de/X/OneHandTehePelo.vrca";
        public static string appPath = Application.dataPath;
        public const string analyzedPlayerPath = @"X\Lists\AnalyzedPlayers\";
    }
}