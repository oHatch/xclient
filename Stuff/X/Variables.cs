﻿using System;
using System.Collections.Generic;
using UnityEngine;
using VRC;
using VRCSDK2;
using VRC.Core;

namespace X.Stuff.X
{
    public class Variables : MonoBehaviour
    {
        public static Rect MainWindowRect;

        public static float oldMainWindowRectHeight;

        public static WWW wwwAudio, wwwAssetBundle;

        public static bool ShowMenu, ShaderSwitchList, ShaderSwitchAll, MeshParticles,
            Particles, GameObject, LineRenderer, Light, DynamicBones, Mesh, Cloth, ShowOptions,
                ShowPlayers, ShowMisc,ShowAPIUsernames, ShowNormalUsernames, BoxESP, simpleAimBot, 
                    playDespacito, checkAvatar, ShowAimbotMenu, autoWalk;

        public static Camera playerCam;
        public static LocomotionInputController currentInputController;
        public static PlayerModComponentJump playerModcomponentJump;
        public static VRC.Player playerAimbot;

        public static AudioSource audioSource;
        public static AudioClip audioClip;

        public static ApiAvatar vrcaAvatar;

        public static string avatarIdB;

        public static string GetAssetUrl(VRC.Player player)
        {
            string assetUrl = player.vrcPlayer?.IIAMBAECJEM?.IOMMPLNNLDO?.assetUrl;
            return assetUrl;
        }

        public static string GetAvatarId(VRC.Player player)
        {
            string avatarId = player.vrcPlayer?.IIAMBAECJEM?.IOMMPLNNLDO?.id;
            return avatarId;
        }

        public static bool isFriend(VRC.Player player)
        {
            return player.BOIFKKECKAJ;
        }

        public static bool isLocal(VRC.Player player)
        {
            return player.KIKCLPOGKII;
        }

        public static bool isMaster(VRC.Player player)
        {
            return player.ACICECHEMCL;
        }

        public static bool isDev(VRC.Player player)
        {
            return player.DBLHBDENAGP;
        }

        public static bool isMod(VRC.Player player)
        {
            return player.FNFPNDHLLFK;
        }

        public static Transform GetClosestEnemy(Transform[] enemies, VRC.Player cPlayer)
        {
            Transform bestTarget = null;
            Variables.closestDistanceSqr = Mathf.Infinity;
            Vector3 currentPosition = cPlayer.transform.position;
            foreach (Transform potentialTarget in enemies)
            {
                Vector3 directionToTarget = potentialTarget.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < Variables.closestDistanceSqr)
                {
                    Variables.closestDistanceSqr = dSqrToTarget;
                    bestTarget = potentialTarget;
                }
            }

            return bestTarget;
        }

        public static float closestDistanceSqr = Mathf.Infinity;

        public static GUIStyle Button, Button2, Button3, Label, Label2, Label3, Box, Box2, Box3, WindowBG, ESPRED, ESPGREEN, ESPBLUE;
        public static Texture2D Background, BackgroundLite, Background2, Background3, Background4, PlayerlistBG, 
            PlayerlistBGLite, WindowT, TexColorRed, TexColorGreen, TexColorBlue;
        public static Vector2 scrollView;
        public static Color ButtonLite, ButtonLite2, ButtonLite3, BoxESP2DC, BoxESP3DC, LineESPC;

        public static float walkSpeed = 3f, strafeSpeed = 2f, runSpeed = 5f, jumpHeight = 3f;

        public static int photonId;

        public static Dictionary<int, GameObject> AvatarCache = new Dictionary<int, GameObject>();
        public static Dictionary<string, string> AvatarIDCache = new Dictionary<string, string>();
        public static Dictionary<string, string> AvatarIDCacheV2 = new Dictionary<string, string>();
        public static Dictionary<string, string> WorldIDCache = new Dictionary<string, string>();
        public static Dictionary<string, object> Avatars = new Dictionary<string, object>();
        public static Dictionary<string, string> InvalidAvatars = new Dictionary<string, string>();
        public static Dictionary<int, GameObject> AvatarStealCache = new Dictionary<int, GameObject>();

        public static List<string> _checkedIds = new List<string>(100);

        public static object id, aName, description, authorId, authorName, assetUrl, imageUrl, thumbnailImageUrl, releaseStatus;

        public static GameObject thisGO;
    }
}
