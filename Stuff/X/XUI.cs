﻿using UnityEngine;
using static UnityEngine.Screen;

namespace X.Stuff.X
{
    public class XUI
    {
        public static void InitializeWindow()
        {
            Variables.MainWindowRect = new Rect(150, 150, width * .4f, height * .53f);
        }

        public static void InitializeBGs()
        {
            Variables.WindowT = new Texture2D(1, 1);
            Variables.WindowT.SetPixels(new[] { new Color(0.1f, 0.1f, 0.1f, 0.75f) });
            Variables.WindowT.Apply();

            Variables.Background = new Texture2D(1, 1);
            Variables.Background.SetPixels(new[] { new Color(0.11f, 0.53f, 0.93f, 0.25f) });
            Variables.Background.Apply();

            Variables.Background2 = new Texture2D(1, 1);
            Variables.Background2.SetPixels(new[] { new Color(0f, 0.93f, 0f, 0.25f) });
            Variables.Background2.Apply();

            Variables.Background3 = new Texture2D(1, 1);
            Variables.Background3.SetPixels(new[] { new Color(1f, 0.19f, 0.19f, 0.25f) });
            Variables.Background3.Apply();

            Variables.Background4 = new Texture2D(1, 1);
            Variables.Background4.SetPixels(new[] { new Color(1f, 1f, 0.05f, 0.35f) });
            Variables.Background4.Apply();

            Variables.PlayerlistBG = new Texture2D(1, 1);
            Variables.PlayerlistBG.SetPixels(new[] { new Color(0.35f, 0.35f, 0.35f, 0.35f) });
            Variables.PlayerlistBG.Apply();

            Variables.PlayerlistBGLite = new Texture2D(1, 1);
            Variables.PlayerlistBGLite.SetPixels(new[] { new Color(0.40f, 0.40f, 0.40f, 0.35f) });
            Variables.PlayerlistBGLite.Apply();

            Variables.BackgroundLite = new Texture2D(1, 1);
            Variables.BackgroundLite.SetPixels(new[] { new Color(0.15f, 0.57f, 0.98f, 0.26f) });
            Variables.BackgroundLite.Apply();

            Variables.TexColorRed = new Texture2D(1, 1);
            Variables.TexColorRed.SetPixels(new[] { new Color(1f, 0f, 0f, 0.5f) });
            Variables.TexColorRed.Apply();

            Variables.TexColorGreen = new Texture2D(1, 1);
            Variables.TexColorGreen.SetPixels(new[] { new Color(0f, 1f, 0f, 0.5f) });
            Variables.TexColorGreen.Apply();

            Variables.TexColorBlue = new Texture2D(1, 1);
            Variables.TexColorBlue.SetPixels(new[] { new Color(0f, 0f, 1f, 0.5f) });
            Variables.TexColorBlue.Apply();

            Variables.ButtonLite = new Color(0.75f, 0.94f, 1f, 1f);
            Variables.ButtonLite2 = new Color(1f, 0.42f, 0.42f, 1f);
            Variables.ButtonLite3 = new Color(0.56f, 0.93f, 0.56f, 1f);

            Variables.BoxESP2DC = Color.green;
            Variables.BoxESP3DC = Color.green;
            Variables.LineESPC = Color.green;
        }

        public static void InitializeUI()
        {
            Variables.Button = new GUIStyle(GUI.skin.button);
            Variables.Button2 = new GUIStyle(GUI.skin.button);
            Variables.Button3 = new GUIStyle(GUI.skin.button);
            Variables.Label = new GUIStyle(GUI.skin.label);
            Variables.Label2 = new GUIStyle(GUI.skin.label);
            Variables.Label3 = new GUIStyle(GUI.skin.label);
            Variables.Box = new GUIStyle(GUI.skin.box);
            Variables.Box2 = new GUIStyle(GUI.skin.box);
            Variables.Box3 = new GUIStyle(GUI.skin.box);
            Variables.WindowBG = new GUIStyle(GUI.skin.box);
            Variables.ESPRED = new GUIStyle(GUI.skin.button);
            Variables.ESPGREEN = new GUIStyle(GUI.skin.button);
            Variables.ESPBLUE = new GUIStyle(GUI.skin.button);
        }

        public static void ApplySkins()
        {
            Variables.WindowBG.normal.background = Variables.WindowT;
            Variables.WindowBG.active.background = Variables.WindowT;
            Variables.WindowBG.hover.background = Variables.WindowT;

            Variables.Box.normal.background = Variables.Background;
            Variables.Box.fontSize = 24;

            Variables.Box2.normal.background = Variables.PlayerlistBG;
            Variables.Box2.fontSize = 20;

            Variables.Label.normal.background = Variables.Background4;
            Variables.Label.fontSize = 20;

            Variables.Label2.normal.background = Variables.PlayerlistBG;
            Variables.Label2.alignment = TextAnchor.MiddleCenter;
            Variables.Label2.fontSize = 20;

            Variables.Label3.normal.background = Variables.PlayerlistBG;
            Variables.Label3.alignment = TextAnchor.MiddleLeft;
            Variables.Label3.fontSize = 20;

            Variables.Button.normal.background = Variables.PlayerlistBG;
            Variables.Button.hover.background = Variables.PlayerlistBGLite;
            Variables.Button.hover.textColor = Variables.ButtonLite2;
            Variables.Button.fontSize = 20;

            Variables.Button2.normal.background = Variables.Background;
            Variables.Button2.hover.background = Variables.BackgroundLite;
            Variables.Button2.hover.textColor = Variables.ButtonLite;
            Variables.Button2.fontSize = 20;
            Variables.Button2.fontStyle = FontStyle.Bold;

            Variables.Button3.normal.background = Variables.Background3;
            Variables.Button3.fontSize = 20;

            Variables.ESPRED.normal.background = Variables.TexColorRed;
            Variables.ESPRED.hover.background = Variables.TexColorRed;
            Variables.ESPRED.fontSize = 20;
            Variables.ESPRED.fontStyle = FontStyle.Bold;

            Variables.ESPGREEN.normal.background = Variables.TexColorGreen;
            Variables.ESPGREEN.hover.background = Variables.TexColorGreen;
            Variables.ESPGREEN.fontSize = 20;
            Variables.ESPGREEN.fontStyle = FontStyle.Bold;

            Variables.ESPBLUE.normal.background = Variables.TexColorBlue;
            Variables.ESPBLUE.hover.background = Variables.TexColorBlue;
            Variables.ESPBLUE.fontSize = 20;
            Variables.ESPBLUE.fontStyle = FontStyle.Bold;
        }
    }
}
