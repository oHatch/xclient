﻿using UnityEngine;
using VRC;
using VRC.Core;
using System;
using System.IO;

namespace X.Stuff.X
{
    public class Miscellaneous
    {
        public static void KickMethodV1(Player player)
        {
            ModerationManager.NNBGGAFMJDN.TryKickUser(player.DPHNAOBNDJD);
            Debug.Log("Executed KMV1");
        }

        public static void KickMethodV2(Player player, APIUser apiUser)
        {
            ModerationManager.NNBGGAFMJDN.UserKickUser(APIUser.CurrentUser, "instance owner", player.DPHNAOBNDJD);
            Debug.Log("Executed KMV2 | " + player.name + " | " + apiUser.displayName);
        }

        public static void KickMethodV3(Player player)
        {
            ModerationManager.NNBGGAFMJDN.ModKickUser(player.DPHNAOBNDJD);
            Debug.Log("Executed KMV3 | " + player.name);
        }

        public static void VoteKickV1(Player player)
        {
            ModerationManager.NNBGGAFMJDN.InitiateVoteToKick(player.DPHNAOBNDJD);
            Debug.Log("Executed VKV1 | " + player.name);
        }

        public static void VoteKickV2(Player player)
        {
            ModerationManager.NNBGGAFMJDN.SendVoteKick(APIUser.CurrentUser.id, player.FOOBPHBOFBL, true);
            Debug.Log("Executed VKV2 | " + player.name);
        }

        public static void WarnUser(Player player)
        {
            ModerationManager.NNBGGAFMJDN.UserWarnUser(APIUser.CurrentUser, "instance owner", player.DPHNAOBNDJD);
            Debug.Log("Executed WarnUser | " + player.name);
        }

        public static void LogOutMethodV1(Player player)
        {
            ModerationManager.NNBGGAFMJDN.ForceUserLogout(player.DPHNAOBNDJD);
            Debug.Log("Executed LogOutV1 | " + player.name);
        }

        public static void BlockPlayer(string userId)
        {
            for (int i = 0; i < 1; i++)
            {
                if (ModerationManager.NNBGGAFMJDN.IsBlocked(userId))
                    ModerationManager.NNBGGAFMJDN.UnblockUser(userId);

                else
                    ModerationManager.NNBGGAFMJDN.BlockUser(userId);
            }
        }

        public static void MuteUser(Player player, string userId)
        {
            for (int i = 0; i < 1; i++)
            {
                if (ModerationManager.NNBGGAFMJDN.IsMuted(userId))
                    ModerationManager.NNBGGAFMJDN.UnmuteUser(userId);

                else
                    ModerationManager.NNBGGAFMJDN.MuteUser(player.DPHNAOBNDJD);
            }
        }

        public static string GetDateAndTime()
        {
            DateTime time = DateTime.Now;
            return time.ToString("yyyy-MM-dd_HH:mm:ss");
        }
    }
}
