﻿using System;
using System.Collections.Generic;
using System.Threading;
using VRC;
using VRC.Core;
using X.Stuff;
using X.Stuff.X;
using X.Stuff.Avatars;
using X.Stuff.XESP;
using UnityEngine;
using System.IO;
using System.Collections;

namespace X.Stuff.X
{
    public class Functions : MonoBehaviour
    {
        public static void NewButtonValue(string content, ref float value, int min = 0, int max = 255)
        {
            GUILayout.BeginVertical();
            try
            {
                GUILayout.Label("<b>" + content + "</b>", Variables.Label2);
                GUILayout.BeginHorizontal();
                try
                {
                    if (GUILayout.Button("<b>-</b>", Variables.Button, GUILayout.Width(35)))
                        value--;
                    GUILayout.Box(value.ToString(), Variables.Box2);
                    if (GUILayout.Button("<b>+</b>", Variables.Button, GUILayout.Width(35)))
                        value++;

                    value = Mathf.Clamp(value, min, max);
                }
                finally
                {
                    GUILayout.EndHorizontal();
                }
            }
            finally
            {
                GUILayout.EndVertical();
            }
        }

        public static void ListPlayer(Player player, string displayName)
        {
            GUILayout.BeginHorizontal();
            try
            {
                Variables.photonId = player.GNFAJLIMCBI() ?? default(int);
                GUILayout.Label(" ID: " + Variables.photonId.ToString() + " ", Variables.Label, GUILayout.ExpandWidth(false));
                PlayerListTags(player);
                GUILayout.Label(" " + displayName, Variables.Label3);
                if (!Variables.isLocal(player))
                {
                    if (GUILayout.Button("<b>S</b>", Variables.Button, GUILayout.Width(25)))
                    {
                        if (player.vrcPlayer != null)
                        {
                            AvatarUtilities.SaveAvatar(player.vrcPlayer, Random.RandomName());
                            Debug.Log("<color=green>Saved Avatar from: " + displayName + "</color>");
                        }
                    }
                    if (GUILayout.Button("<b>TP</b>", Variables.Button, GUILayout.Width(35)))
                    {
                        Player p = PlayerManager.GetCurrentPlayer();
                        Teleport(p.transform, player.transform);
                    }
                    if (GUILayout.Button("<b>B</b>", Variables.Button, GUILayout.Width(25)))
                    {
                        Miscellaneous.BlockPlayer(player.FOOBPHBOFBL);
                    }
                    if (GUILayout.Button("<b>M</b>", Variables.Button, GUILayout.Width(27)))
                    {
                        Miscellaneous.MuteUser(player, player.FOOBPHBOFBL);
                    }
                    if (GUILayout.Button("<b>A</b>", Variables.Button, GUILayout.Width(27)))
                    {
                        Variables.simpleAimBot = !Variables.simpleAimBot;
                        Variables.playerAimbot = player;
                    }
                    if (GUILayout.Button("<b>C</b>", Variables.Button, GUILayout.Width(27)))
                    {
                        Avatar.ScanAndLimit(player.vrcPlayer?.IIAMBAECJEM.currentAvatar, player.DPHNAOBNDJD.displayName);
                    }
                    if (GUILayout.Button("<b>L</b>", Variables.Button, GUILayout.Width(27)))
                    {
                        Functions.ListAllShaders(player.vrcPlayer?.IIAMBAECJEM?.currentAvatar);
                    }
                }
                /*if (GUILayout.Button("T", Variables.Button, GUILayout.Width(35)))
                {
                    Miscellaneous.KickMethodV1(player);
                }
                if (GUILayout.Button("T2", Variables.Button, GUILayout.Width(45)))
                {
                    Miscellaneous.KickMethodV2(player, player.DPHNAOBNDJD);
                }
                if (GUILayout.Button("T3", Variables.Button, GUILayout.Width(45)))
                {
                    Miscellaneous.KickMethodV3(player);
                }
                if (GUILayout.Button("T4", Variables.Button, GUILayout.Width(45)))
                {
                    Miscellaneous.WarnUser(player);
                }
                if (GUILayout.Button("T5", Variables.Button, GUILayout.Width(45)))
                {
                    Miscellaneous.VoteKickV1(player);
                }
                if (GUILayout.Button("T6", Variables.Button, GUILayout.Width(45)))
                {
                    Miscellaneous.VoteKickV2(player);
                }
                if (GUILayout.Button("T7", Variables.Button, GUILayout.Width(45)))
                {
                    Miscellaneous.LogOutMethodV1(player);
                }*/
            }
            finally
            {
                GUILayout.EndHorizontal();
            }
        }

        public static void NewToggleButtonM2(string text, ref bool toggle)
        {
            string isEnabled = toggle ? "<color=green>Enabled" : "<color=red>Disabled";

            GUILayout.BeginHorizontal();
            try
            {
                GUILayout.Label(" " + text, Variables.Label3, GUILayout.Width(Variables.MainWindowRect.width * .2f));
                if (GUILayout.Button($"{isEnabled}</color>", Variables.Button))
                    toggle = !toggle;
            }
            finally
            {
                GUILayout.EndHorizontal();
            }
        }

        public static void NewToggleButton(string text, ref bool toggle)
        {
            string isEnabled = toggle ? "<color=green>Enabled" : "<color=red>Disabled";

            GUILayout.BeginHorizontal();
            try
            {
                GUILayout.Label(" " + text, Variables.Label3, GUILayout.Width(Variables.MainWindowRect.width * .4f));
                if (GUILayout.Button($"{isEnabled}</color>", Variables.Button))
                    toggle = !toggle;
            }
            finally
            {
                GUILayout.EndHorizontal();
            }
        }

        public static void PlayerListTags(Player p)
        {
            try
            {
                if (Variables.isMaster(p) && !Variables.isLocal(p) && !Variables.isFriend(p))
                    GUILayout.Label("<b><color=green>M</color></b>", Variables.Label2, GUILayout.Width(30));

                else if (Variables.isLocal(p) && !Variables.isMaster(p))
                    GUILayout.Label("<b><color=yellow>L</color></b>", Variables.Label2, GUILayout.Width(30));

                else if (Variables.isLocal(p) && Variables.isMaster(p))
                {
                    GUILayout.Label("<b><color=green>M</color></b>", Variables.Label2, GUILayout.Width(30));
                    GUILayout.Label("<b><color=yellow>L</color></b>", Variables.Label2, GUILayout.Width(30));
                }

                else if (Variables.isMod(p) || Variables.isDev(p))
                    GUILayout.Label("<b><color=red>Dev</color></b>", Variables.Label2, GUILayout.Width(60));

                else if (Variables.isMaster(p) && Variables.isMod(p) || Variables.isDev(p))
                {
                    GUILayout.Label("<b><color=red>Dev</color></b>", Variables.Label2, GUILayout.Width(60));
                    GUILayout.Label("<b><color=green>M</color></b>", Variables.Label2, GUILayout.Width(30));
                }

                else if (Variables.isFriend(p) && Variables.isMaster(p))
                {
                    GUILayout.Label("<b><color=green>M</color></b>", Variables.Label2, GUILayout.Width(30));
                    GUILayout.Label("<b><color=green>F</color></b>", Variables.Label2, GUILayout.Width(30));
                }

                else if (Variables.isFriend(p) && !Variables.isMaster(p))
                    GUILayout.Label("<b><color=green>F</color></b>", Variables.Label2, GUILayout.Width(30));
            }
            catch (Exception) { }
        }

        public static void OnMainWindowOpen(int id)
        {
            GUILayout.Box("<b>Naatiivee's <i>XClient</i></b>", Variables.Box);

            GUILayout.BeginArea(new Rect(5, 43, Variables.MainWindowRect.width - 10, Variables.MainWindowRect.height - 30));
            try
            {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Options", Variables.Button2))
                {
                    Variables.ShowOptions = true;
                    Variables.ShowMisc = false;
                    Variables.ShowPlayers = false;
                    if (Variables.oldMainWindowRectHeight == Variables.MainWindowRect.height)
                        Variables.MainWindowRect.height = Variables.MainWindowRect.height + 30f;
                }
                if (GUILayout.Button("Miscellaneous", Variables.Button2))
                {
                    Variables.ShowMisc = true;
                    Variables.ShowOptions = false;
                    Variables.ShowPlayers = false;
                    Variables.MainWindowRect.height = Variables.oldMainWindowRectHeight;
                }
                if (GUILayout.Button("STASI", Variables.Button2))
                {
                    Variables.ShowOptions = false;
                    Variables.ShowPlayers = true;
                    Variables.ShowMisc = false;
                    if (Variables.oldMainWindowRectHeight == Variables.MainWindowRect.height)
                     Variables.MainWindowRect.height = Variables.MainWindowRect.height + 100f;
                }
                GUILayout.EndHorizontal();
                if (Variables.ShowOptions && !Variables.ShowPlayers && !Variables.ShowMisc)
                {
                    NewToggleButton("Shader Switch List", ref Variables.ShaderSwitchList);
                    NewToggleButton("Shader Switch All", ref Variables.ShaderSwitchAll);
                    NewToggleButton("Mesh Particles", ref Variables.MeshParticles);
                    NewToggleButton("Dynamic Bones", ref Variables.DynamicBones);
                    NewToggleButton("LineRenderer", ref Variables.LineRenderer);
                    NewToggleButton("GameObject", ref Variables.GameObject);
                    NewToggleButton("Particles", ref Variables.Particles);
                    NewToggleButton("Light", ref Variables.Light);
                    NewToggleButton("Cloth", ref Variables.Cloth);
                    NewToggleButton("Mesh", ref Variables.Mesh);
                    if (GUILayout.Button("Manual run Anti(s)", Variables.Button2))
                        Variables.AvatarCache.Clear();
                    GUILayout.BeginHorizontal();
                    NewToggleButtonM2("2D Box ESP", ref ESPType.boundingBoxEspOn2D);
                    NewToggleButtonM2("3D Box ESP", ref ESPType.boundingBoxEspOn3D);
                    NewToggleButtonM2("Line ESP", ref ESPType.traceLineEspOn);
                    GUILayout.EndHorizontal();
                    GUILayout.Space(10);
                    GUILayout.BeginHorizontal();
                    NewButtonValue("Walk Speed", ref Variables.walkSpeed, 0, 255);
                    GUILayout.Space(5);
                    NewButtonValue("Run Speed", ref Variables.runSpeed, 0, 255);
                    GUILayout.Space(5);
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("Set", Variables.Button))
                    {
                        Functions.SetWalkSpeed();
                    }
                    GUILayout.Space(5);
                    if (GUILayout.Button("Set", Variables.Button))
                    {
                        Functions.SetRunSpeed();
                    }
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                    NewButtonValue("Strafe Speed", ref Variables.strafeSpeed, 0, 255);
                    GUILayout.Space(5);
                    NewButtonValue("Jump Height", ref Variables.jumpHeight, 0, 255);
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("Set", Variables.Button))
                    {
                        Functions.SetStrafeSpeed();
                    }
                    GUILayout.Space(5);
                    if (GUILayout.Button("Set", Variables.Button))
                    {
                        Functions.SetJumpHeight();
                    }
                    GUILayout.EndHorizontal();
                }
                if (!Variables.ShowOptions && Variables.ShowPlayers && !Variables.ShowMisc)
                {
                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("API Usernames", Variables.Button2))
                    {
                        Variables.ShowAPIUsernames = true;
                        Variables.ShowNormalUsernames = false;
                    }
                    if (GUILayout.Button("Normal Usernames", Variables.Button2))
                    {
                        Variables.ShowNormalUsernames = true;
                        Variables.ShowAPIUsernames = false;
                    }
                    GUILayout.EndHorizontal();
                    Variables.scrollView = GUILayout.BeginScrollView(Variables.scrollView);
                    foreach (Player player in PlayerManager.GetAllPlayers())
                    {
                        if (player != null)
                        {
                            string name = Variables.ShowAPIUsernames ? player.DPHNAOBNDJD?.displayName : player.name;
                            ListPlayer(player, name);
                        }
                    }
                    GUILayout.EndScrollView();
                    if (GUILayout.Button("Set Masterclient", Variables.Button2))
                    {
                        APMDPMJMOCD.AHIIEAIDHCE(APMDPMJMOCD.PEALLCAIILJ);
                    }
                    GUILayout.Space(14);
                }
                if (Variables.ShowMisc && !Variables.ShowPlayers && !Variables.ShowOptions)
                {
                    GUILayout.Space(5);
                    if (GUILayout.Button("Tell Alexa to play Despacito", Variables.Button2))
                    {
                        Variables.playDespacito = !Variables.playDespacito;
                    }
                    if (GUILayout.Button("Tell Alexa to stop playing Despacito", Variables.Button2))
                    {
                        Variables.audioSource.Stop();
                    }
                    GUILayout.Space(10);
                    Functions.ESPColorPicker();
                    GUILayout.Space(10);
                    if (GUILayout.Button("Analyze all Players", Variables.Button2))
                    {
                        foreach (Player pTA in PlayerManager.GetAllPlayers())
                        {
                            Avatars.Analyze.ThisPlayer(pTA);
                        }
                    }
                }
            }
            finally
            {
                GUILayout.EndArea();
            }

            GUI.DragWindow(new Rect(0, 0, 10000, 25));
        }

        public static void WriteLogs()
        {
            new Thread(() =>
            {
                foreach (KeyValuePair<string, string> kvp in Variables.AvatarIDCache)
                {
                    string avtr_id = kvp.Key;
                    string userID = kvp.Value;
                    if (File.ReadAllText(Constants.avatarLogPath).Contains(avtr_id))
                        continue;
                    else
                    {
                        File.AppendAllText(Constants.avatarLogPath, userID + " | " + avtr_id + " | " + @"https://api.vrchat.cloud/api/1/avatars/" + avtr_id + @"?apiKey=JlE5Jldo5Jibnk5O5hTx6XVqsJu4WJ26" + Environment.NewLine);
                        Debug.Log("<color=yellow>Wrote: </color>" + userID + " | " + avtr_id + " | " + @"https://api.vrchat.cloud/api/1/avatars/" + avtr_id + @"?apiKey=JlE5Jldo5Jibnk5O5hTx6XVqsJu4WJ26" + "<color=yellow> into text file!</color>");
                    }
                }

                foreach (KeyValuePair<string, string> kvp2 in Variables.AvatarIDCacheV2)
                {
                    string avatarId = kvp2.Key;
                    string userId = kvp2.Value;
                    if (File.ReadAllText(Constants.avatarLogV2Path).Contains(avatarId))
                        continue;
                    else
                    {
                        File.AppendAllText(Constants.avatarLogV2Path, userId + " | " + avatarId + " | " + @"https://api.vrchat.cloud/api/1/avatars/" + avatarId + @"?apiKey=JlE5Jldo5Jibnk5O5hTx6XVqsJu4WJ26" + Environment.NewLine);
                        Debug.Log("<color=yellow>Wrote: </color>" + userId + " | " + avatarId + " | " + @"https://api.vrchat.cloud/api/1/avatars/" + avatarId + @"?apiKey=JlE5Jldo5Jibnk5O5hTx6XVqsJu4WJ26" + "<color=yellow> into text file!</color>");
                    }
                }

                foreach (KeyValuePair<string, string> kvp4 in Variables.InvalidAvatars)
                {
                    try
                    {
                        string avatarId = kvp4.Key;
                        string userId = kvp4.Value;
                        if (File.ReadAllText(Constants.invalidAvatarsLogPath).Contains(avatarId))
                            continue;
                        else
                        {
                            File.AppendAllText(Constants.invalidAvatarsLogPath, userId + " | " + avatarId + " | " + @"https://api.vrchat.cloud/api/1/avatars/" + avatarId + @"?apiKey=JlE5Jldo5Jibnk5O5hTx6XVqsJu4WJ26" + Environment.NewLine);
                            Debug.Log("<color=yellow>Wrote: </color>" + userId + " | " + avatarId + " | " + @"https://api.vrchat.cloud/api/1/avatars/" + avatarId + @"?apiKey=JlE5Jldo5Jibnk5O5hTx6XVqsJu4WJ26" + "<color=yellow> into text file!</color>");
                        }
                    }
                    catch(Exception E)
                    {
                        Debug.Log(E);
                    }
                }

                /*foreach (KeyValuePair<string, string> kvp3 in Variables.WorldIDCache)
                {
                    string worldId = kvp3.Key;
                    string worldName = kvp3.Value;
                    if (File.ReadAllText(Constants.worldLogPath).Contains(worldId))
                        continue;
                    else
                    {
                        File.AppendAllText(Constants.worldLogPath, worldId + " | " + worldName + Environment.NewLine);
                        Debug.Log("<color=yellow>Wrote: " + worldId + " | " + worldName + " into text file!</color>");
                    }
                }*/
            }).Start();
        }

        public static void Teleport(Transform from, Transform to)
        {
            from.position = to.position;
            from.rotation = to.rotation;
        }

        public static void TeleportPug()
        {
            Vector3 tpPos = new Vector3(0f, -0.2869983f, 1.254509e-08f);
            Player p = PlayerManager.GetCurrentPlayer();
            p.transform.position = tpPos;
        }

        public static void ToggleParticleSystems(GameObject avatar)
        {
            ParticleSystem[] particleSystems = avatar.GetComponentsInChildren<ParticleSystem>(true);
            for (int x = 0; x < particleSystems.Length; x++)
            {
                if (particleSystems[x].isPlaying)
                {
                    particleSystems[x].Stop(true);
                    if (particleSystems[x].GetComponent<ParticleSystemRenderer>())
                    {
                        ParticleSystemRenderer particleSystemRenderers = particleSystems[x].GetComponent<ParticleSystemRenderer>();
                        particleSystemRenderers.enabled = false;
                    }
                }
                else
                {
                    if (particleSystems[x].GetComponent<ParticleSystemRenderer>())
                    {
                        if (!particleSystems[x].GetComponent<ParticleSystemRenderer>().enabled)
                        {
                            ParticleSystemRenderer particleSystemRenderers2 = particleSystems[x].GetComponent<ParticleSystemRenderer>();
                            particleSystemRenderers2.enabled = true;
                        }
                    }
                }
            }
        }

        public static void SetWalkSpeed()
        {
            Variables.currentInputController.walkSpeed = Variables.walkSpeed;
            Debug.Log("Walk Speed set to: " + Variables.walkSpeed);
        }

        public static void SetRunSpeed()
        {
            Variables.currentInputController.runSpeed = Variables.runSpeed;
            Debug.Log("Run Speed set to: " + Variables.runSpeed);
        }

        public static void SetStrafeSpeed()
        {
            Variables.currentInputController.strafeSpeed = Variables.strafeSpeed;
            Debug.Log("Strafe Speed set to: " + Variables.strafeSpeed);
        }

        public static void SetJumpHeight()
        {
            if (PlayerManager.GetCurrentPlayer().GetComponentInChildren<PlayerModComponentJump>())
            {
                Player player = PlayerManager.GetCurrentPlayer();
                if (player != null)
                {
                    Variables.playerModcomponentJump = player.GetComponentInChildren<PlayerModComponentJump>();
                    Variables.playerModcomponentJump.HBNGGKBGDIO = Variables.jumpHeight;
                }
            }
            else
            {
                Player player = PlayerManager.GetCurrentPlayer();
                if (player != null)
                {
                    player.vrcPlayer?.gameObject.AddComponent<PlayerModComponentJump>();
                    Variables.playerModcomponentJump = player.GetComponentInChildren<PlayerModComponentJump>();
                    Variables.playerModcomponentJump.HBNGGKBGDIO = Variables.jumpHeight;
                }
            }
        }

        public static void executeAimBot(Player player)
        {
            try
            {
                player = Variables.playerAimbot;
                GameObject gameObject = PlayerManager.GetCurrentPlayer()?.vrcPlayer?.IIAMBAECJEM?.currentAvatar;
                Animator animator = gameObject.GetComponent<Animator>();

                if (animator != null)
                {
                    Transform transF = animator.GetBoneTransform(HumanBodyBones.Head);
                    transF.LookAt(player.vrcPlayer?.transform);
                }
            }
            catch (Exception ex) { Debug.Log(ex); }
        }

        public static void EnableAdminAccess()
        {
            Transform aAT = GameObject.Find("DrPotato~<3").transform;
            int childCount = aAT.childCount;
            Debug.Log(childCount);

            Transform next = aAT.GetChild(3);
            int childCount2 = next.childCount;
            Debug.Log(childCount2);

            Transform nextChild = next.GetChild(1);
            int childCount3 = nextChild.childCount;
            Debug.Log(childCount3);

            Transform deletePanel = nextChild.GetChild(3);

            deletePanel.gameObject.SetActive(false);
            next.gameObject.SetActive(true);
            nextChild.gameObject.SetActive(true);
        }

        public static void JoinWorldInstance(string id_instance)
        {
            try
            {
                ApiWorld currentWorld = RoomManagerBase.currentRoom;
                if (currentWorld != null)
                {
                    Debug.Log("Current world: " + currentWorld.name + " | WorldID: " + currentWorld.id);
                    VRCFlowManager.NNBGGAFMJDN.EnterWorld(id_instance, null, null, true);
                }
            }
            catch(Exception T) { Debug.Log(T); }
        }

        public static void CreateNewWorldInstance(string id_instance)
        {
            try
            {
                VRCFlowManager.NNBGGAFMJDN.EnterNewWorldInstance(id_instance, null, null);
            }
            catch(Exception Z){ Debug.Log(Z); }
        }

        public static void CreateNewWorldInstanceV2(string id_instance)
        {
            try
            {
                APMDPMJMOCD.DDIFKMDDDKI(id_instance);
            }
            catch (Exception I) { Debug.Log(I); }
        }

        public static void GoHome()
        {
            VRCFlowManager.NNBGGAFMJDN.GoHome();
        }

        public static IEnumerator AlexaLoadDespacito(string path)
        {
            using (Variables.wwwAudio = new WWW(path))
            {
                Debug.LogAssertion("LOADING DESPACITO!");
                yield return Variables.wwwAudio;
                Variables.audioClip = Variables.wwwAudio.GetAudioClip(false);
                Debug.LogAssertion("LOADED DESPACITO!");
            }
            VRCPlayer.Instance.gameObject.AddComponent<AudioSource>();
            Variables.audioSource = VRCPlayer.Instance.gameObject.GetComponent<AudioSource>();
            Debug.LogAssertion("FOUND VRCPLAYER!");
            if (Variables.audioClip != null)
            {
                Variables.audioSource.clip = Variables.audioClip;
                Variables.audioSource.PlayOneShot(Variables.audioClip, 1f);
                Debug.LogAssertion("PLAYING MUSIC!");
            }
        }

        public static void ListAllShaders(GameObject avatar)
        {
            Renderer[] renderers = avatar.GetComponentsInChildren<Renderer>(true);
            foreach (Renderer rend in renderers)
            {
                Material[] materials = rend.materials;
                foreach (Material mat in materials)
                {
                    Debug.Log(mat.shader);
                    Debug.Log(mat.shader.name + Environment.NewLine);
                }
            }
        }

        public static void ESPColorPicker()
        {
            GUILayout.Label("ESP Color Picker", Variables.Label2);
            GUILayout.Space(5);
            GUILayout.Label("2D BoxESP Color", Variables.Label2);
            GUILayout.BeginHorizontal();
            if(GUILayout.Button("Red", Variables.ESPRED))
            {
                Variables.BoxESP2DC = Color.red;
            }
            if (GUILayout.Button("Green", Variables.ESPGREEN))
            {
                Variables.BoxESP2DC = Color.green;
            }
            if (GUILayout.Button("Blue", Variables.ESPBLUE))
            {
                Variables.BoxESP2DC = Color.blue;
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(5);
            GUILayout.Label("3D BoxESP Color", Variables.Label2);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Red", Variables.ESPRED))
            {
                Variables.BoxESP3DC = Color.red;
            }
            if (GUILayout.Button("Green", Variables.ESPGREEN))
            {
                Variables.BoxESP3DC = Color.green;
            }
            if (GUILayout.Button("Blue", Variables.ESPBLUE))
            {
                Variables.BoxESP3DC = Color.blue;
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(5);
            GUILayout.Label("LineESP Color", Variables.Label2);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Red", Variables.ESPRED))
            {
                Variables.LineESPC = Color.red;
            }
            if (GUILayout.Button("Green", Variables.ESPGREEN))
            {
                Variables.LineESPC = Color.green;
            }
            if (GUILayout.Button("Blue", Variables.ESPBLUE))
            {
                Variables.LineESPC = Color.blue;
            }
            GUILayout.EndHorizontal();
        }

        public static void AutoWalk()
        {

        }
    }
}