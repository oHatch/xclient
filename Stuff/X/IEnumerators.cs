﻿using System.Collections;
using UnityEngine;
using VRC.Core;

namespace X.Stuff.X
{
    public class IEnumerators
    {
        public static IEnumerator LoadAvatar(string avatarUrl, int avatarVersion, VRC.Player player)
        {
            WWW www = WWW.LoadFromCacheOrDownload(avatarUrl, avatarVersion);
            yield return www;

            Debug.Log("Download finished");

            AssetBundle bundle = www.assetBundle;
            AssetBundleRequest request = bundle.LoadAssetWithSubAssetsAsync("assets/_customavatar.prefab");
            yield return request;

            Debug.Log("Finished loading!");

            GameObject avatar = request.asset as GameObject;

            bundle.Unload(false);
            www.Dispose();
        }
    }
}