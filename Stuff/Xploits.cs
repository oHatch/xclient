﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRC;
using VRC.Core;
using ExitGames.Client.Photon;
using MoPhoGames.USpeak.Core;
using MoPhoGames.USpeak.Interface;

namespace X.Stuff
{
    public class Xploits : VRCPunBehaviour
    {
        public static HGMNBHMOMJC TargetActor(int photonId)
        {
            HGMNBHMOMJC target = null;
            foreach (var player in PlayerManager.GetAllPlayers())
            {
                if (player.GNFAJLIMCBI() == photonId)
                    target = player.HGMNBHMOMJC;
            }
            return target;
        }

        public static void LogOut(byte[] myByte, int byteModifier, List<Player> vrcPlayerList, ushort convertedBytes)
        {
            USpeakPhotonSender3D newSender = new USpeakPhotonSender3D();
            newSender.DLDJGJDIDBP((byte[])myByte, (int)byteModifier, (List<VRC.Player>)vrcPlayerList, (ushort)convertedBytes);
        }

        public static List<VRC.Player> PlayerList()
        {
            List<VRC.Player> playerList = PlayerManager.GetAllPlayersWithinRange(VRCPlayer.Instance.transform.position, 50f, 64);
            return playerList;
        }
    }
}
