﻿using System.Linq;
using System.Reflection;
using VRC;
using UnityEngine;
using System;

namespace X.Stuff
{
    public class Flying : MonoBehaviour
    {
        private bool flyMode;

        private LocomotionInputController inputController;
        private VRCMotionState motionState;
        private Vector3 originalGravity;

        public void Awake()
        {
            DontDestroyOnLoad(this);
        }

        public void Update()
        {
            if (APMDPMJMOCD.IPPOKDOLGFC)
            {
                try
                {
                    if (Event.current.shift && Input.GetKeyDown(KeyCode.F))
                    {
                        flyMode = !flyMode;

                        if (flyMode)
                        {
                            originalGravity = Physics.gravity;
                            Physics.gravity = Vector3.zero;
                        }
                        else
                            Physics.gravity = originalGravity;
                    }

                    if (Event.current.shift && Input.GetKeyDown(KeyCode.M))
                    {
                        string status = string.Empty;
                        Collider[] array = UnityEngine.Object.FindObjectsOfType<Collider>();
                        Component[] components = VRCPlayer.Instance.GetComponents(typeof(Collider));
                        foreach (Collider collider in array)
                        {
                            if (collider != components.ElementAt(0))
                            {
                                collider.enabled = !collider.enabled;
                                status = (collider.enabled ? "OFF" : "ON");
                                bool enabled = collider.enabled;
                            }
                        }
                        Debug.Log("NoClip is turned " + status);
                    }

                    bool isShift = Input.GetKey(KeyCode.LeftShift);
                    int speed = isShift ? 12 : 8;
                    if (flyMode)
                    {
                        VRCPlayer currentPlayer = PlayerManager.GetCurrentPlayer()?.vrcPlayer;

                        if (currentPlayer != null)
                        {
                            if (inputController == null)
                            {
                                inputController = currentPlayer.GetComponentInChildren<LocomotionInputController>();
                                motionState = inputController.OIIMFBGGJBC;
                            }

                            motionState.Reset();
                            Vector3 oldPos = currentPlayer.transform.position;

                            if (Input.GetKey(KeyCode.Q))
                                currentPlayer.transform.position = new Vector3(oldPos.x, oldPos.y - (speed * Time.deltaTime), oldPos.z);
                            else if (Input.GetKey(KeyCode.E))
                                currentPlayer.transform.position = new Vector3(oldPos.x, oldPos.y + (speed * Time.deltaTime), oldPos.z);
                        }
                    }

                    inputController.strafeSpeed = flyMode ? speed : speed / 2;
                    inputController.runSpeed = flyMode ? 12 : 8;
                }
                catch (Exception)
                { }
            }
        }
    }
}