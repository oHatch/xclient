﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using VRC;
using VRC.Core;
using X.Stuff;
using X.Stuff.X;

namespace X.Stuff
{
    public static class AvatarUtilities
    {
        public static void Init(this ApiAvatar avatar, string id, APIUser user, string name, string imageUrl, string assetUrl, string description, string releaseStatus, List<string> tags, string packageUrl = null)
        {
            avatar.id = id;
            avatar.authorName = user.displayName;
            avatar.authorId = user.id;
            avatar.name = name;
            avatar.assetUrl = assetUrl;
            avatar.imageUrl = imageUrl;
            avatar.description = description;
            avatar.releaseStatus = releaseStatus;
            avatar.tags = tags;
            avatar.unityPackageUrl = packageUrl;
        }

        public static ApiAvatar SaveAvatar(VRCPlayer vrcPlayer, string name, string imageUrl = "")
        {
            return AvatarUtilities.SaveAvatar(vrcPlayer.AGOJEGCJLJD, name, imageUrl);
        }

        public static ApiAvatar SaveAvatar(ApiAvatar avatar, string name, string imageUrl = "")
        {
            Debug.Log("Saving avatar...");
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);
            using (WebClient webClient = new WebClient())
            {
                string tempFile =  Constants.VRCAPath + name + ".vrca";
                try
                {
                    webClient.DownloadFile(avatar.assetUrl, tempFile);
                    ApiFileHelper.UploadFileAsync(tempFile, null, Guid.NewGuid().ToString(), delegate (ApiFile apiFile, string message)
                    {
                        apiFile.Get(delegate
                        {
                            ApiAvatar apiAvatar = new ApiAvatar();
                            apiAvatar.Init(null, APIUser.CurrentUser, name, avatar.imageUrl, apiFile.GetFileURL(), avatar.description, "private", null);
                            apiAvatar.Save(delegate (ApiContainer a)
                            {
                                string str = string.Format("Successfully saved Avatar: {0}. {1}", name + Environment.NewLine, a.Text);
                                Debug.Log(str);
                            }, delegate (ApiContainer fa)
                            {
                                string str2 = string.Format("Error saving avatar: {0}. {1}." + Environment.NewLine, fa.Error + Environment.NewLine, fa.Text);
                                Debug.Log(str2);
                            });
                        }, delegate (ApiContainer f)
                        {
                            string str3 = string.Format("Error saving avatar: {0}. {1}." + Environment.NewLine, f.Error + Environment.NewLine, f.Text);
                            Debug.Log(str3);
                        });
                    }, delegate (ApiFile apiFile, string error)
                    {
                        string str4 = string.Format("Error saving avatar: {0}" + Environment.NewLine, error);
                        Debug.Log(str4);
                    }, delegate
                    {
                    }, (ApiFile _) => false);
                }
                catch (Exception ex)
                {
                    string str5 = string.Format("Error saving avatar: {0}" + Environment.NewLine, ex.Message);
                    Debug.Log(str5);
                }
            }
            return avatar;
        }
    }
}