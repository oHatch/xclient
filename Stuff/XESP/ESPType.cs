﻿using System;
using UnityEngine;
using VRC;
using VRC.Core;
using X.Stuff.X;

namespace X.Stuff.XESP
{
    public class ESPType : MonoBehaviour
    {
        public void DrawGUI()
        {
            if (this.menuVisible)
            {
                GUISkin guiskin = GUI.skin;
                GUI.skin = this.skin;
                this.windowRect = GUI.Window(0, this.windowRect, new GUI.WindowFunction(this.DrawGUIWindow), "DEM ESP THO");
                GUI.skin = guiskin;
            }
        }

        public void DrawGUIWindow(int id)
        {
            int num = 22;
            skin.label.fontSize = 12;
            boundingBoxEspOn2D = GUI.Toggle(new Rect(0f, (float)num, 150f, 20f), boundingBoxEspOn2D, "2D Bounding Box ESP");
            if (boundingBoxEspOn2D && boundingBoxEspOn3D)
            {
                boundingBoxEspOn3D = false;
            }
            num += 22;
            boundingBoxEspOn3D = GUI.Toggle(new Rect(0f, (float)num, 150f, 20f), boundingBoxEspOn3D, "3D Bounding Box ESP");
            if (boundingBoxEspOn2D && boundingBoxEspOn3D)
            {
                boundingBoxEspOn2D = false;
            }
            num += 22;
            nameEspOn = GUI.Toggle(new Rect(0f, (float)num, 150f, 20f), nameEspOn, "Name ESP");
            num += 22;
            traceLineEspOn = GUI.Toggle(new Rect(0f, (float)num, 150f, 20f), traceLineEspOn, "Trace Line ESP");
            num += 22;
            skin.label.fontSize = 10;
            GUI.Label(new Rect(36.5f, (float)num, 125f, 20f), "ESP Menu");
        }

        public void DrawTraceLines()
        {
            Player[] allPlayers = PlayerManager.GetAllPlayers();
            for (int i = 0; i < allPlayers.Length; i++)
            {
                APIUser apiuser = allPlayers[i].DPHNAOBNDJD;
                if (apiuser != null && !(apiuser.id == APIUser.CurrentUser.id))
                {
                    Vector3 position = allPlayers[i].transform.position;
                    Vector3 vector = VRCVrCamera.GetInstance().screenCamera.WorldToScreenPoint(position);
                    if (vector.z > 0f)
                    {
                        Vector3 vector2 = GUIUtility.ScreenToGUIPoint(vector);
                        vector2.y = (float)Screen.height - vector2.y;
                        Color color = Variables.LineESPC;
                        ESPHelper.DrawLine(new Vector2((float)(Screen.width / 2), (float)(Screen.height / 2)), vector2, color);
                    }
                }
            }
        }

        public void Draw3DBoundingBoxes()
        {
            foreach (Player player in PlayerManager.GetAllPlayers())
            {
                APIUser apiuser = player.DPHNAOBNDJD;
                if (apiuser != null && !(apiuser.id == APIUser.CurrentUser.id))
                {
                    Color c = Variables.BoxESP3DC;
                    Vector3 position = player.transform.position;
                    Vector3 vector = position;
                    vector.y += 1.7f;
                    Vector3 min = new Vector3(position.x - 0.5f, position.y, position.z - 0.5f);
                    Vector3 max = new Vector3(position.x + 0.5f, vector.y, position.z + 0.5f);
                    this.Draw3DBox(position, min, max, Quaternion.Euler(player.transform.rotation.eulerAngles), c);
                }
            }
        }

        public void Draw3DBox(Vector3 center, Vector3 min, Vector3 max, Quaternion rot, Color c)
        {
            Vector3 vector = new Vector3(min.x, max.y, min.z);
            Vector3 vector2 = new Vector3(max.x, max.y, min.z);
            Vector3 vector3 = new Vector3(min.x, max.y, max.z);
            Vector3 vector4 = new Vector3(min.x, min.y, max.z);
            Vector3 vector5 = new Vector3(max.x, min.y, min.z);
            Vector3 vector6 = new Vector3(max.x, min.y, max.z);
            min = RotateAroundPoint(min, center, rot);
            max = RotateAroundPoint(max, center, rot);
            vector = RotateAroundPoint(vector, center, rot);
            vector2 = RotateAroundPoint(vector2, center, rot);
            vector3 = RotateAroundPoint(vector3, center, rot);
            vector4 = RotateAroundPoint(vector4, center, rot);
            vector5 = RotateAroundPoint(vector5, center, rot);
            vector6 = RotateAroundPoint(vector6, center, rot);
            Vector3 vector7 = VRCVrCamera.GetInstance().screenCamera.WorldToScreenPoint(min);
            Vector3 vector8 = VRCVrCamera.GetInstance().screenCamera.WorldToScreenPoint(max);
            Vector3 vector9 = VRCVrCamera.GetInstance().screenCamera.WorldToScreenPoint(vector);
            Vector3 vector10 = VRCVrCamera.GetInstance().screenCamera.WorldToScreenPoint(vector2);
            Vector3 vector11 = VRCVrCamera.GetInstance().screenCamera.WorldToScreenPoint(vector3);
            Vector3 vector12 = VRCVrCamera.GetInstance().screenCamera.WorldToScreenPoint(vector4);
            Vector3 vector13 = VRCVrCamera.GetInstance().screenCamera.WorldToScreenPoint(vector5);
            Vector3 vector14 = VRCVrCamera.GetInstance().screenCamera.WorldToScreenPoint(vector6);
            if (vector7.z > 0f && vector8.z > 0f && vector9.z > 0f && vector10.z > 0f && vector11.z > 0f && vector12.z > 0f && vector13.z > 0f && vector14.z > 0f)
            {
                Vector3 vector15 = GUIUtility.ScreenToGUIPoint(vector7);
                vector15.y = (float)Screen.height - vector15.y;
                Vector3 vector16 = GUIUtility.ScreenToGUIPoint(vector8);
                vector16.y = (float)Screen.height - vector16.y;
                Vector3 vector17 = GUIUtility.ScreenToGUIPoint(vector9);
                vector17.y = (float)Screen.height - vector17.y;
                Vector3 vector18 = GUIUtility.ScreenToGUIPoint(vector10);
                vector18.y = (float)Screen.height - vector18.y;
                Vector3 vector19 = GUIUtility.ScreenToGUIPoint(vector11);
                vector19.y = (float)Screen.height - vector19.y;
                Vector3 vector20 = GUIUtility.ScreenToGUIPoint(vector12);
                vector20.y = (float)Screen.height - vector20.y;
                Vector3 vector21 = GUIUtility.ScreenToGUIPoint(vector13);
                vector21.y = (float)Screen.height - vector21.y;
                Vector3 vector22 = GUIUtility.ScreenToGUIPoint(vector14);
                vector22.y = (float)Screen.height - vector22.y;
                ESPHelper.DrawLine(vector15, vector21, c);
                ESPHelper.DrawLine(vector15, vector20, c);
                ESPHelper.DrawLine(vector22, vector21, c);
                ESPHelper.DrawLine(vector22, vector20, c);
                ESPHelper.DrawLine(vector17, vector18, c);
                ESPHelper.DrawLine(vector17, vector19, c);
                ESPHelper.DrawLine(vector16, vector18, c);
                ESPHelper.DrawLine(vector16, vector19, c);
                ESPHelper.DrawLine(vector15, vector17, c);
                ESPHelper.DrawLine(vector20, vector19, c);
                ESPHelper.DrawLine(vector21, vector18, c);
                ESPHelper.DrawLine(vector22, vector16, c);
            }
        }

        public void Draw2DBoundingBoxes()
        {
            Player[] allPlayers = PlayerManager.GetAllPlayers();
            for (int i = 0; i < allPlayers.Length; i++)
            {
                APIUser apiuser = allPlayers[i].DPHNAOBNDJD;
                if (apiuser != null && !(apiuser.id == APIUser.CurrentUser.id))
                {
                    Vector3 position = allPlayers[i].transform.position;
                    Vector3 position2 = position;
                    position2.y += 1.7f;
                    Vector3 vector = VRCVrCamera.GetInstance().screenCamera.WorldToScreenPoint(position);
                    Vector3 vector2 = VRCVrCamera.GetInstance().screenCamera.WorldToScreenPoint(position2);
                    if (vector.z > 0f && vector2.z > 0f)
                    {
                        Vector3 vector3 = GUIUtility.ScreenToGUIPoint(vector);
                        vector3.y = (float)Screen.height - vector3.y;
                        Vector3 vector4 = GUIUtility.ScreenToGUIPoint(vector2);
                        vector4.y = (float)Screen.height - vector4.y;
                        float num = Math.Abs(vector3.y - vector4.y) / 2.2f / 2f;
                        Vector3 v = new Vector3(vector4.x - num, vector4.y);
                        Vector3 v2 = new Vector3(vector4.x + num, vector4.y);
                        Vector3 v3 = new Vector3(vector4.x - num, vector3.y);
                        Vector3 v4 = new Vector3(vector4.x + num, vector3.y);
                        Color color = Variables.BoxESP2DC;
                        ESPHelper.DrawLine(v, v2, color);
                        ESPHelper.DrawLine(v, v3, color);
                        ESPHelper.DrawLine(v3, v4, color);
                        ESPHelper.DrawLine(v2, v4, color);
                    }
                }
            }
        }

        public void DrawNames()
        {
            foreach (Player player in PlayerManager.GetAllPlayers())
            {
                APIUser apiuser = player.DPHNAOBNDJD;
                if (apiuser != null && !(apiuser.id == APIUser.CurrentUser.id))
                {
                    Vector3 position = player.transform.position;
                    position.y += 2.5f;
                    Vector3 vector = VRCVrCamera.GetInstance().screenCamera.WorldToScreenPoint(position);
                    if (vector.z > 0f)
                    {
                        Vector3 vector2 = GUIUtility.ScreenToGUIPoint(vector);
                        vector2.y = (float)Screen.height - vector2.y;
                        GUI.Label(new Rect(vector2.x, vector2.y, 125f, 25f), "<size=10><b><color=magenta>" + apiuser.displayName + "</color></b></size>");
                    }
                }
            }
        }

        public static Texture2D CreatePlainTexture(float r, float g, float b, float a)
        {
            Texture2D texture2D = new Texture2D(1, 1);
            texture2D.SetPixel(0, 0, new Color(r, g, b, a));
            texture2D.Apply();
            return texture2D;
        }

        public static Vector3 RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle)
        {
            return angle * (point - pivot) + pivot;
        }

        public void OnGUI()
        {
            if (boundingBoxEspOn2D)
            {
                Draw2DBoundingBoxes();
            }
            if (boundingBoxEspOn3D)
            {
                Draw3DBoundingBoxes();
            }
            if (nameEspOn)
            {
                DrawNames();
            }
            if (traceLineEspOn)
            {
                DrawTraceLines();
            }
            this.DrawGUI();
        }

        public void Start()
        {
            this.skin = new GUISkin();
            this.skin.horizontalSlider.normal.background = CreatePlainTexture(0.5f, 0.5f, 0.5f, 0.4f);
            this.skin.horizontalSlider.onNormal.background = CreatePlainTexture(0.5f, 0.5f, 0.5f, 0.4f);
            this.skin.horizontalSlider.active.background = CreatePlainTexture(0.5f, 0.5f, 0.5f, 0.4f);
            this.skin.horizontalSlider.onActive.background = CreatePlainTexture(0.5f, 0.5f, 0.5f, 0.4f);
            this.skin.horizontalSlider.focused.background = CreatePlainTexture(0.5f, 0.5f, 0.5f, 0.4f);
            this.skin.horizontalSlider.onFocused.background = CreatePlainTexture(0.5f, 0.5f, 0.5f, 0.4f);
            this.skin.horizontalSliderThumb.fixedHeight = 5f;
            this.skin.horizontalSliderThumb.fixedWidth = 5f;
            this.skin.horizontalSliderThumb.normal.background = CreatePlainTexture(0f, 0f, 0f, 0.4f);
            this.skin.horizontalSliderThumb.onNormal.background = CreatePlainTexture(0f, 0f, 0f, 0.4f);
            this.skin.horizontalSliderThumb.active.background = CreatePlainTexture(0f, 0f, 0f, 0.4f);
            this.skin.horizontalSliderThumb.onActive.background = CreatePlainTexture(0f, 0f, 0f, 0.4f);
            this.skin.horizontalSliderThumb.focused.background = CreatePlainTexture(0f, 0f, 0f, 0.4f);
            this.skin.horizontalSliderThumb.onFocused.background = CreatePlainTexture(0f, 0f, 0f, 0.4f);
            this.skin.toggle.normal.background = CreatePlainTexture(0.5f, 0.5f, 0.5f, 0.4f);
            this.skin.toggle.onNormal.background = CreatePlainTexture(1f, 1f, 1f, 0.4f);
            this.skin.toggle.active.background = CreatePlainTexture(1f, 1f, 1f, 0.4f);
            this.skin.toggle.onActive.background = CreatePlainTexture(1f, 1f, 1f, 0.4f);
            this.skin.toggle.padding = new RectOffset(5, 0, 3, 0);
            this.skin.textField.normal.background = CreatePlainTexture(0f, 0f, 0f, 0.4f);
            this.skin.textField.onNormal.background = CreatePlainTexture(0f, 0f, 0f, 0.4f);
            this.skin.textField.active.background = CreatePlainTexture(0f, 0f, 0f, 0.4f);
            this.skin.textField.onActive.background = CreatePlainTexture(0f, 0f, 0f, 0.4f);
            this.skin.textField.focused.background = CreatePlainTexture(0f, 0f, 0f, 0.4f);
            this.skin.textField.onFocused.background = CreatePlainTexture(0f, 0f, 0f, 0.4f);
            this.skin.textField.fontSize = 10;
            this.skin.window.normal.background = CreatePlainTexture(1f, 1f, 1f, 0.7f);
            this.skin.window.onNormal.background = CreatePlainTexture(1f, 1f, 1f, 0.7f);
            this.skin.window.active.background = CreatePlainTexture(1f, 1f, 1f, 0.7f);
            this.skin.window.onActive.background = CreatePlainTexture(1f, 1f, 1f, 0.7f);
            this.skin.window.padding = new RectOffset(5, 0, 3, 0);
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Insert))
            {
                this.menuVisible = !this.menuVisible;
            }
        }

        private Rect windowRect = new Rect(10f, 350f, 150f, 125f);

        private GUISkin skin;

        private bool menuVisible;

        public static bool boundingBoxEspOn2D;

        public static bool boundingBoxEspOn3D;

        public static bool nameEspOn;

        public static bool traceLineEspOn;
    }
}