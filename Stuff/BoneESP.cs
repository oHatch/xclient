﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRC;
using VRC.Core;
using UnityEngine;

namespace X.Stuff
{
    public class BoneESP : MonoBehaviour
    {
        private Texture2D _tex;
        private List<string> _checkedIds = new List<string>(100);
        public bool _enabled = true;
        public bool _showNames = false;

        public void Update()
        {
            if (Event.current.control && Input.GetKeyDown(KeyCode.O))
                    _enabled = !_enabled;

            if (Event.current.control && Input.GetKeyDown(KeyCode.L))
                _showNames = !_showNames;
        }

        public void OnGUI()
        {
            if (RoomManagerBase.currentRoom == null || APIUser.CurrentUser == null)
            {
                if (_checkedIds.Count != 0)
                    _checkedIds.Clear();
                return;
            }

            if (_tex == null)
                _tex = new Texture2D(1, 1);

            Camera cam = VRCVrCamera.GetInstance().screenCamera;
            foreach (Player p in PlayerManager.GetAllPlayers())
            {
                ApiAvatar userAvatar = p.vrcPlayer?.AGOJEGCJLJD;
                APIUser user = p.DPHNAOBNDJD;

                if (user == null || user.id == APIUser.CurrentUser.id)
                    continue;

                if (_enabled)
                {
                    GUI.skin.label.alignment = TextAnchor.MiddleCenter;
                    bool drewName = false;

                    Animator animator = p.vrcPlayer?.IIAMBAECJEM?.currentAvatar?.GetComponent<Animator>();
                    if (animator != null)
                    {
                        for (int i = 0; i < 56; i++)
                        {
                            HumanBodyBones bone = (HumanBodyBones)i;
                            Transform boneTransform = animator.GetBoneTransform(bone);
                            if (boneTransform == null)
                                continue;

                            Vector3 bonePosition = boneTransform.position;
                            if (Vector3.Dot(cam.transform.forward, bonePosition - cam.transform.position) > 0)
                            {
                                Vector3 boneScreenPosition = cam.WorldToScreenPoint(bonePosition);
                                Graphics.DrawTexture(new Rect(boneScreenPosition.x, Screen.height - boneScreenPosition.y, 2, 2), _tex);
                                if (_showNames && bone == HumanBodyBones.Head)
                                {
                                    bonePosition.y += 0.25f;
                                    boneScreenPosition = cam.WorldToScreenPoint(bonePosition);
                                    GUI.Label(new Rect(boneScreenPosition.x - 50, Screen.height - boneScreenPosition.y - 50, 100, 100), user.displayName, GUI.skin.label);
                                    drewName = true;
                                }
                            }
                        }
                    }

                    if (_showNames && !drewName)
                    {
                        Vector3 playerPosition = p.vrcPlayer.transform.position;
                        if (Vector3.Dot(cam.transform.forward, playerPosition - cam.transform.position) > 0)
                        {
                            Vector3 playerSreenPosition = cam.WorldToScreenPoint(playerPosition);
                            GUI.Label(new Rect(playerSreenPosition.x - 50, Screen.height - playerSreenPosition.y - 50, 100, 100), user.displayName, GUI.skin.label);
                        }
                    }
                }
            }
        }
    }
}
