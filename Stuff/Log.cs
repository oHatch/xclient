﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using UnityEngine;
using VRC;
using VRC.Core;
using X.Stuff.X;
using X.Stuff;

namespace X.Stuff
{
    public class Log
    {
        public static void World()
        {
            ApiWorld currentWorld = RoomManagerBase.currentRoom;
            try
            {
                if (currentWorld != null)
                {
                    string worldId = currentWorld.id;
                    string name = currentWorld.name;
                    string description = currentWorld.description;
                    string authorId = currentWorld.authorId;
                    string authorName = currentWorld.authorName;
                    string capacity = currentWorld.capacity.ToString();
                    string releaseStatus = currentWorld.releaseStatus;
                    string imageUrl = currentWorld.imageUrl;
                    string thumbnailImageUrl = currentWorld.thumbnailImageUrl;
                    string assetUrl = currentWorld.assetUrl;
                    string version = currentWorld.version.ToString();
                    string worldRGUID = @"worldG_" + Guid.NewGuid().ToString();

                    string WorldAPIURL = @"http://localhost/worldapi.php?id=" + worldId + "&name=" + name + "&description=" + description + "&authorId=" + authorId + "&authorName=" + authorName + "&capacity=" + capacity + "&tags=tag&releaseStatus=" + releaseStatus + "&imageUrl=" + imageUrl + "&thumbnailImageUrl=" + thumbnailImageUrl + "&assetUrl=" + assetUrl + "&version=" + version + "&worldRGUID=" + worldRGUID;
                    WorldAPIURL = WorldAPIURL.Replace(" ", "%20");

                    WebClient webClient = new WebClient();
                    string yikes = webClient.DownloadString(WorldAPIURL);
                    Debug.Log(yikes + " | WorldAPI");
                    webClient.Dispose();
                }
            }
            catch (Exception exx) { Debug.Log(exx);
                Debug.Log(exx.Message);
            }
        }

        public static void Player()
        {
            foreach (Player player in PlayerManager.GetAllPlayers())
            {
                try
                {
                    if (player == null)
                        continue;
                    if (player.DPHNAOBNDJD.id == APIUser.CurrentUser.id)
                        continue;
                    else
                    {
                        string userId = player.DPHNAOBNDJD.id;
                        string username = player.DPHNAOBNDJD.username;
                        string displayName = player.DPHNAOBNDJD.displayName;
                        string currentAvatarcurrentAvatarThumbnailImageUrl = player.DPHNAOBNDJD.currentAvatarThumbnailImageUrl;
                        string developerType = player.DPHNAOBNDJD.developerType.ToString();
                        string steamId = player.HGMNBHMOMJC.FBNFJEHIFPJ["steamUserID"].ToString();
                        string playerRGUID = @"playerG_" + Guid.NewGuid().ToString();

                        string PlayerAPIURL = @"http://localhost/playerapi.php?id=" + userId + "&username=" + username + "&displayName=" + displayName + "&currentAvatarThumbnailImageUrl=" + currentAvatarcurrentAvatarThumbnailImageUrl + "&developerType=" + developerType + "&tags=tag&status=active&steamId=" + steamId + "&playerRGUID=" + playerRGUID;
                        PlayerAPIURL = PlayerAPIURL.Replace(" ", "%20");

                        WebClient client = new WebClient();
                        string playerLog = client.DownloadString(PlayerAPIURL);
                        Debug.Log(playerLog + " | PlayerAPI");
                        client.Dispose();
                    }
                }
                catch (Exception ex) { Debug.Log(ex);
                    Debug.Log(ex.Message);
                }
            }
        }

        public static void Avatar()
        {
            foreach (Player player in PlayerManager.GetAllPlayers())
            {
                try
                {
                    if (player == null)
                        continue;
                    if (player.DPHNAOBNDJD.id == APIUser.CurrentUser.id)
                        continue;
                    else
                    {
                        string avatarRGUID = @"avatarG_" + Guid.NewGuid().ToString();

                        ApiAvatar apiAvatar = player.vrcPlayer?.IIAMBAECJEM?.IOMMPLNNLDO;
                        if (player.vrcPlayer.IIAMBAECJEM.IOMMPLNNLDO != null)
                        {
                            string id = apiAvatar.id;
                            string name = apiAvatar.name;
                            string description = apiAvatar.description;
                            string authorId = apiAvatar.authorId;
                            string authorName = apiAvatar.authorName;
                            string assetUrl = apiAvatar.assetUrl;
                            string imageUrl = apiAvatar.imageUrl;
                            string thumbnailImageUrl = apiAvatar.thumbnailImageUrl;
                            string releaseStatus = apiAvatar.releaseStatus;

                            string AvatarAPIUrl2 = @"http://localhost/avatarapi.php?id=" + id + "&name=" + name + "&description=" + description + "&authorId=" + authorId + "&authorName=" + authorName + "&tags=tag&assetUrl=" + assetUrl + "&imageUrl=" + imageUrl + "&thumbnailImageUrl=" + thumbnailImageUrl + "&releaseStatus=" + releaseStatus + "&avatarRGUID=" + avatarRGUID;
                            AvatarAPIUrl2 = AvatarAPIUrl2.Replace(" ", "%20");

                            Debug.Log(AvatarAPIUrl2);
                        }

                        Variables.Avatars = player.HGMNBHMOMJC.FBNFJEHIFPJ["avatarDict"] as Dictionary<string, object>;
                        if (player.HGMNBHMOMJC.FBNFJEHIFPJ != null)
                        {
                            Variables.Avatars.TryGetValue("id", out Variables.id);
                            Variables.Avatars.TryGetValue("name", out Variables.aName);
                            Variables.Avatars.TryGetValue("description", out Variables.description);
                            Variables.Avatars.TryGetValue("authorId", out Variables.authorId);
                            Variables.Avatars.TryGetValue("authorName", out Variables.authorName);
                            Variables.Avatars.TryGetValue("assetUrl", out Variables.assetUrl);
                            Variables.Avatars.TryGetValue("imageUrl", out Variables.imageUrl);
                            Variables.Avatars.TryGetValue("thumbnailImageUrl", out Variables.thumbnailImageUrl);
                            Variables.Avatars.TryGetValue("releaseStatus", out Variables.releaseStatus);

                            string AvatarAPIUrl = @"http://localhost/avatarapi.php?id=" + Variables.id + "&name=" + Variables.aName + "&description=" + Variables.description + "&authorId=" + Variables.authorId + "&authorName=" + Variables.authorName + "&tags=tag&assetUrl=" + Variables.assetUrl + "&imageUrl=" + Variables.imageUrl + "&thumbnailImageUrl=" + Variables.thumbnailImageUrl + "&releaseStatus=" + Variables.releaseStatus + "&avatarRGUID=" + avatarRGUID;
                            AvatarAPIUrl = AvatarAPIUrl.Replace(" ", "%20");

                            WebClient webC = new WebClient();
                            string avatarLog = webC.DownloadString(AvatarAPIUrl);
                            Debug.Log(avatarLog + " | AvatarAPI");
                            webC.Dispose();
                        }
                    }
                }
                catch (Exception x) { Debug.Log(x);
                    Debug.Log(x.Message);
                }
            }
        }
    }
}
