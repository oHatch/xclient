﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.IO;
using VRC;
using VRC.Core;
using X.Stuff.X;
using ExitGames.Client.Photon;

namespace X.Stuff
{
    public class Caching : MonoBehaviour
    {
        public static IEnumerator ScanAll()
        {
            while (true)
            {
                if (APMDPMJMOCD.IPPOKDOLGFC)
                {
                    var players = PlayerManager.GetAllPlayers();
                    if (players.Length > 0)
                    {
                        foreach (var playr in players)
                        {
                            if (playr == null)
                                continue;
                            int photonID = playr.GNFAJLIMCBI() ?? default(int);
                            if (Variables.AvatarCache.ContainsKey(photonID))
                            {
                                var avatar = playr.vrcPlayer?.IIAMBAECJEM?.currentAvatar;
                                if (avatar == null)
                                    Variables.AvatarCache.Remove(photonID);
                                else if (Variables.AvatarCache[photonID] != avatar)
                                    Variables.AvatarCache.Remove(photonID);
                            }
                            else
                            {
                                var avatar = playr.vrcPlayer?.IIAMBAECJEM?.currentAvatar;
                                if (avatar != null)
                                {
                                    string displayName = playr.DPHNAOBNDJD?.displayName;
                                    if (displayName != null)
                                        Avatar.ScanAndLimit(avatar, displayName);
                                    var clone = playr.GetComponentInChildren<VRCAvatarManager>();
                                    if (clone)
                                    {
                                        var clones = clone.ReturnClones();
                                        if (clones[0] != null)
                                            Avatar.ScanAndLimit(clones[0], clones[0].name);
                                        if (clones[1] != null)
                                            Avatar.ScanAndLimit(clones[1], clones[1].name);
                                        yield return new WaitForSeconds(0.1f);
                                    }
                                    Variables.AvatarCache[photonID] = avatar;
                                }
                            }
                            yield return new WaitForSeconds(0.1f);
                        }
                    }
                }
                yield return new WaitForEndOfFrame();
            }
        }

        public static IEnumerator CacheAvatarIDs()
        {
            while (true)
            {

                if (APMDPMJMOCD.IPPOKDOLGFC)
                {
                    var players = PlayerManager.GetAllPlayers();
                    if (players.Length > 0)
                    {
                        foreach (var playr in players)
                        {
                            try
                            {
                                if (playr == null || playr.vrcPlayer == null)
                                    continue;
                                string avtr_id = (playr.vrcPlayer?.IIAMBAECJEM?.IOMMPLNNLDO as ApiAvatar).id;
                                string userID = playr.HGMNBHMOMJC?.FBNFJEHIFPJ["userId"].ToString();
                                if (Variables.AvatarIDCache.ContainsKey(avtr_id) && avtr_id != null && userID != null)
                                    continue;
                                if (!playr.KIKCLPOGKII && avtr_id != null && userID != null)
                                {
                                    Variables.AvatarIDCache[avtr_id] = userID;
                                }
                            }
                            catch (Exception) { }

                            yield return new WaitForSeconds(0.1f);
                        }
                    }
                }
                yield return new WaitForEndOfFrame();
            }
        }

        public static IEnumerator CacheAvatarIDsV2()
        {
            while (true)
            {
                if (APMDPMJMOCD.IPPOKDOLGFC)
                {
                    var players2 = PlayerManager.GetAllPlayers();
                    if (players2.Length > 0)
                    {
                        foreach (var playr in players2)
                        {
                            try
                            {
                                if (playr == null && !playr.GetComponentInChildren<VRCAvatarManager>())
                                    continue;
                                var clone = playr.GetComponentInChildren<VRCAvatarManager>();
                                string avatarId = clone.KGFDALOENOG;
                                string userId = playr.FOOBPHBOFBL;
                                if (Variables.AvatarIDCacheV2.ContainsKey(avatarId) && avatarId != null && userId != null)
                                    continue;
                                if (!playr.KIKCLPOGKII && avatarId != null && userId != null)
                                {
                                    Variables.AvatarIDCacheV2[avatarId] = userId;
                                }
                            }
                            catch (Exception) { }

                            yield return new WaitForSeconds(0f);
                        }
                    }
                }
                yield return new WaitForEndOfFrame();
            }
        }

        public static IEnumerator CacheWorldIDs()
        {
            while (true)
            {
                if (APMDPMJMOCD.IPPOKDOLGFC)
                {
                    var apiWorld = RoomManagerBase.currentRoom;
                    try
                    {
                        if (apiWorld == null)
                            continue;
                        string worldName = apiWorld.name;
                        string worldId = apiWorld.id;
                        if (Variables.WorldIDCache.ContainsKey(worldId) && worldName != null && worldId != null)
                            continue;
                        if (worldName != null && worldId != null)
                        {
                            Variables.WorldIDCache[worldId] = worldName;
                        }
                    }
                    catch (Exception x) { Debug.Log(x); }

                    yield return new WaitForSeconds(0f);
                }
                yield return new WaitForEndOfFrame();
            }
        }
    }
}