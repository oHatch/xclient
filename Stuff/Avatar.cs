﻿using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using VRC;
using VRC.Core;
using X.Stuff.X;

namespace X.Stuff
{
    public class Avatar : MonoBehaviour
    {
        private static string url = "https://cloud.naatiivee-works.de/VRC-OAC/Lists/Shaders.txt",
                                 url2 = "https://cloud.naatiivee-works.de/VRC-OAC/Lists/GameObjects.txt",
                                     url3 = "https://cloud.naatiivee-works.de/VRC-OAC/Lists/AudioClips.txt";

        private static string shaderList, gameObjectList, audioClipList;

        public static IEnumerator GetListsFromWebsite()
        {
            using (WWW www = new WWW(url))
            {
                yield return www;
                shaderList = www.text;
            }
            using (WWW www2 = new WWW(url2))
            {
                yield return www2;
                gameObjectList = www2.text;
            }
            using (WWW www3 = new WWW(url3))
            {
                yield return www3;
                audioClipList = www3.text;
            }
        }

        public static void ScanAndLimit(GameObject avatar, string displayName)
        {
            var local = PlayerManager.GetCurrentPlayer();
            if (local != null && local.vrcPlayer?.IIAMBAECJEM?.currentAvatar == avatar)
            {
                Debug.Log("<color=green> CHECKED MYSELF </color>");
            }
            else if(displayName.ToLower().Contains("shadowclone") || displayName.ToLower().Contains("mirrorclone"))
            {
                Debug.Log("<color=green> CHECKED CLONE " + displayName + "</color>");
            }
            else
            {
                if (Variables.Particles)
                    Functions.ToggleParticleSystems(avatar);

                if (Variables.ShaderSwitchAll && !Variables.ShaderSwitchList)
                {
                    Renderer[] renderers = avatar.GetComponentsInChildren<Renderer>(true);
                    foreach (Renderer rend in renderers)
                    {
                        Material[] materials = rend.materials;
                        foreach (Material mat in materials)
                        {
                            if (mat.shader.name != "Standard" || mat.shader != Shader.Find("Standard"))
                            {
                                Debug.Log("Switched " + mat.shader.name + " to Standard!");
                                mat.shader = Shader.Find("Standard");
                            }
                        }
                        Debug.Log("Switched all shaders from Player " + displayName + " to Standard!");
                    }

                    ParticleSystem[] particleSystems = avatar.GetComponentsInChildren<ParticleSystem>(true);
                    foreach (ParticleSystem particleSystem in particleSystems)
                    {
                        ParticleSystemRenderer particleSystemRenderer = particleSystem.GetComponentInChildren<ParticleSystemRenderer>();
                        Material[] materials = particleSystemRenderer.materials;
                        foreach (Material mat in materials)
                        {
                            if (mat.shader.name.ToLower() != "standard" || mat.shader != Shader.Find("Standard"))
                            {
                                Debug.Log("Switched " + mat.shader.name + " to Standard!");
                                mat.shader = Shader.Find("Standard");
                            }
                        }
                        Debug.Log("Switched all Particle-shaders from Player " + displayName + " to Standard!");
                    }
                }

                if (Variables.ShaderSwitchList && !Variables.ShaderSwitchAll)
                {
                    Renderer[] renderers2 = avatar.GetComponentsInChildren<Renderer>(true);
                    foreach (Renderer rend2 in renderers2)
                    {
                        Material[] materials2 = rend2.materials;
                        foreach (Material mat2 in materials2)
                        {
                            foreach (string shaderN in shaderList.Split(new[] { '\n' }))
                            {
                                if (mat2.shader.name.Contains(shaderN))
                                {
                                    string shadername = mat2.shader.name;
                                    mat2.shader = Shader.Find("Standard");
                                    Debug.Log("Switched " + shadername + " from Player " + displayName + " to Standard!");
                                }
                            }
                        }
                    }
                }

                if (Variables.Light)
                {
                    Light[] lights = avatar.GetComponentsInChildren<Light>(true);
                    for (int x = 0; x < lights.Length; x++)
                    {
                        lights[x].intensity = lights[x].intensity / 2;
                        lights[x].shadows = LightShadows.None;
                        Debug.Log("Limited all lights from Player " + displayName);
                    }
                    if (lights.Length >= 6)
                    {
                        int value = lights.Length;
                        for (int i = 6; i < lights.Length; i++)
                        {
                            Destroy(lights[i]);
                        }
                        Debug.Log("Player " + displayName + " breached Light limit, limiting to 5!  Value: " + value);
                    }
                }

                if (Variables.Cloth)
                {
                    Cloth[] cloths = avatar.GetComponentsInChildren<Cloth>(true);
                    if (cloths.Length >= 6)
                    {
                        int value = cloths.Length;
                        for (int i = 6; i < cloths.Length; i++)
                        {
                            Destroy(cloths[i]);
                        }
                        Debug.Log("Player " + displayName + " breached Cloth limit, limiting to 5!  Value: " + value);
                    }
                }

                if (Variables.MeshParticles)
                {
                    ParticleSystem[] particleSystems = avatar.GetComponentsInChildren<ParticleSystem>(true);
                    for (int x = 0; x < particleSystems.Length; x++)
                    {
                        int value = particleSystems.Length;
                        Debug.Log("Found " + value + " ParticleSystems on Player " + displayName);
                        if (particleSystems[x].GetComponent<ParticleSystemRenderer>())
                        {
                            ParticleSystemRenderer particleSystemRenderers = particleSystems[x].GetComponent<ParticleSystemRenderer>();
                            if (particleSystemRenderers.renderMode == ParticleSystemRenderMode.Mesh)
                            {
                                Mesh[] mmmEsh = new Mesh[0];
                                particleSystemRenderers.GetMeshes(mmmEsh);
                                if (mmmEsh.Length == 0 && particleSystemRenderers.mesh != null)
                                {
                                    mmmEsh = new Mesh[]
                                    {
                                    particleSystemRenderers.mesh
                                    };
                                }
                                foreach (Mesh mesh in mmmEsh)
                                {
                                    if (mesh.vertexCount >= 15000)
                                    {
                                        DestroyImmediate(particleSystems[x]);
                                        Debug.Log("Deleted ParticleSystem " + particleSystems[x].name + " from Player " + displayName);
                                        Debug.Log("Violation: Breached vertices limit!");
                                    }
                                    if (mesh.triangles.Length / 3 >= 20000)
                                    {
                                        DestroyImmediate(particleSystems[x]);
                                        Debug.Log("Deleted ParticleSystem " + particleSystems[x].name + " from Player " + displayName);
                                        Debug.Log("Violation: Breached triangles limit!");
                                    }
                                }
                            }
                        }
                    }
                }

                if (Variables.LineRenderer)
                {
                    LineRenderer[] lineRenderers = avatar.GetComponentsInChildren<LineRenderer>(true);
                    if (lineRenderers.Length >= 6)
                    {
                        int value = lineRenderers.Length;
                        for (int x = 0; x < lineRenderers.Length; x++)
                        {
                            if (lineRenderers[x].startWidth >= 0.11f || lineRenderers[x].endWidth >= 0.11f)
                            {
                                lineRenderers[x].startWidth = 0.1f;
                                lineRenderers[x].endWidth = 0.1f;
                            }
                        }
                        for (int i = 6; i < lineRenderers.Length; i++)
                        {
                            Destroy(lineRenderers[i]);
                        }
                        Debug.Log("Player " + displayName + " breached LineRenderer limit, limiting to 5!  Value: " + value);
                    }
                }

                if (Variables.DynamicBones)
                {
                    DynamicBone[] dynamicBones = avatar.GetComponentsInChildren<DynamicBone>(true);
                    if (dynamicBones.Length >= 15)
                    {
                        int value = dynamicBones.Length;
                        for (int x = 0; x < dynamicBones.Length; x++)
                        {
                            if (dynamicBones[x].m_UpdateRate >= 61)
                                dynamicBones[x].m_UpdateRate = 60f;
                        }
                        for (int i = 15; i < dynamicBones.Length; i++)
                        {
                            Destroy(dynamicBones[i]);
                        }
                        Debug.Log("Player " + displayName + " breached DynamicBone limit, limiting to 15! Value: " + value);
                    }
                }

                if (Variables.Mesh)
                {
                    SkinnedMeshRenderer[] skinnedMeshRenderers = avatar.GetComponentsInChildren<SkinnedMeshRenderer>(true);
                    foreach (SkinnedMeshRenderer sMR in skinnedMeshRenderers)
                    {
                        int vertC = sMR.sharedMesh.vertexCount;
                        int triC = sMR.sharedMesh.triangles.Length / 3;
                        int submC = sMR.sharedMesh.subMeshCount;
                        string meshName = sMR.sharedMesh.name;

                        if (vertC >= 67000)
                        {
                            Destroy(sMR);
                            Debug.Log("Mesh " + meshName + " has been deleted from Player " + displayName);
                            Debug.Log("Violation: Breached vertices limit! Vertices: " + vertC);
                        }
                        if (triC >= 100000)
                        {
                            Destroy(sMR);
                            Debug.Log("Mesh " + meshName + " has been deleted from Player " + displayName);
                            Debug.Log("Violation: Breached triangles limit! Triangles: " + triC);
                        }
                        if (submC >= 100)
                        {
                            Destroy(sMR);
                            Debug.Log("Mesh " + meshName + " has been deleted from Player " + displayName);
                            Debug.Log("Violation: Breached submesh limit! Submeshes: " + submC);
                        }
                    }

                    MeshFilter[] meshFilters = avatar.GetComponentsInChildren<MeshFilter>(true);
                    foreach (MeshFilter mF in meshFilters)
                    {
                        int vertC = mF.sharedMesh.vertexCount;
                        int triC = mF.sharedMesh.triangles.Length / 3;
                        int submC = mF.sharedMesh.subMeshCount;
                        string meshName = mF.sharedMesh.name;

                        if (vertC >= 67000)
                        {
                            Destroy(mF);
                            Debug.Log("Mesh " + meshName + " has been deleted from Player " + displayName);
                            Debug.Log("Violation: Breached vertices limit! Vertices: " + vertC);
                        }
                        if (triC >= 100000)
                        {
                            Destroy(mF);
                            Debug.Log("Mesh " + meshName + " has been deleted from Player " + displayName);
                            Debug.Log("Violation: Breached triangles limit! Triangles: " + triC);
                        }
                        if (submC >= 100)
                        {
                            Destroy(mF);
                            Debug.Log("Mesh " + meshName + " has been deleted from Player " + displayName);
                            Debug.Log("Violation: Breached submesh limit! Submeshes: " + submC);
                        }
                    }
                }
                Debug.Log("<color=green> CHECKED PLAYER " + displayName + "</color>");
            }
        }
    }
}